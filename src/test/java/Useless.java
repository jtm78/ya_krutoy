
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Useless {

    public void navigateToAnywhere() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.google.ru/");

        WebElement searchField = driver.findElement(By.cssSelector("input[type=\"text\"]"));
        searchField.clear();
        searchField.sendKeys("Iphone");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        searchField.submit();
        WebElement link = driver.findElement(By.cssSelector("div.r a h3.LC20lb"));
        link.click();

    }
}

