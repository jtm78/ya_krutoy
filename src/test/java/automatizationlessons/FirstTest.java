package automatizationlessons;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class FirstTest {

    private WebDriver driver;

    public FirstTest(WebDriver driver){
        this.driver = driver;
    }


    public static void main(String[] args) {
        WebDriver driver = iniDriver();
        driver.get("https://www.bing.com/");
        driver.manage().window().maximize();
        WebElement searchField = driver.findElement(By.id("sb_form_q"));
        searchField.sendKeys("selenium");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement searchButton = driver.findElement(By.name("go"));
        searchButton.click();
        List<WebElement> listofLinks = driver.findElements(By.className("b_algo"));
        System.out.println(listofLinks.size());
    }

    public static WebDriver iniDriver() {
        ChromeOptions options = new ChromeOptions();
        return new ChromeDriver();
    }
}
