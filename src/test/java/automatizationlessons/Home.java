package automatizationlessons;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Home {
    private WebDriver driver;

    public Home(ChromeDriver driver) {
        this.driver = driver;

    }

    public ResultPage search(String string) {
        driver.findElement(By.xpath("//input[@class=\"gLFyf gsfi\"]")).sendKeys(string);
        return new ResultPage(driver);
    }
}
