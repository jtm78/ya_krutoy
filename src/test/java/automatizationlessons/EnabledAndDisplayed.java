package automatizationlessons;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

public class EnabledAndDisplayed {

    public static WebElement driver;
    public static   WebElement enabled(WebElement driver) throws InterruptedException {
        EnabledAndDisplayed.driver = driver;
        for(int count = 0;;count++){
            if(count>=10){
                throw new TimeoutException();
            }
            try{
                EnabledAndDisplayed.driver.isDisplayed();
                break;
            }
            catch (NoSuchElementException e){
                Thread.sleep(1000);
            }
        }
        return driver;

    }
}
