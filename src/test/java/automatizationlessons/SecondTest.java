package automatizationlessons;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class SecondTest {
    private ChromeDriver driver;
    @BeforeTest
    public WebDriver getDriver(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.google.ru/");
        return driver;
    }

    public void testSearch(){
        Home home = new Home(driver);
        ResultPage result = home.search("automated testing info");


    }
    @AfterTest
    public void closeDriver(){
        driver.close();
    }
}
