package automatizationlessons;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class WithTestSite {
    public static void main(String[] args) {
        WebDriver driver = new ChromeDriver();
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        WebElement emailField = driver.findElement(By.xpath("//input[@id=\"email\"]"));
        emailField.sendKeys("webinar.test@gmail.com");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        WebElement passwordField = driver.findElement(By.xpath("//input[@id=\"passwd\"]"));
        passwordField.sendKeys("Xcg7299bnSmMuRLp9ITw");
        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);

        WebElement applyButton = driver.findElement(By.xpath("//button[@name=\"submitLogin\" and @tabindex=\"4\"]"));
        applyButton.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        WebElement buttonKatalog = driver.findElement(By.xpath("//li[@id = \"subtab-AdminCatalog\"]/a[@class=\"title has_submenu\"]"));
        buttonKatalog.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);







    }
}
