package automatizationlessons;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverInit {



    private static DriverInit instanceDriver = null;

    private WebDriver driver;

    public void setName(){

    }

    private DriverInit(){
        driver = new ChromeDriver();
    }
    public static DriverInit getInstance(){
        if(instanceDriver == null){
            instanceDriver = new DriverInit();
        }
        return instanceDriver;
    }
    public WebDriver getDriver(){
        return driver;
    }
    public static void chromedisplayed(WebElement locatorone) throws InterruptedException {

        for(int count = 0;;count++){
            if(count>=10){
                throw new TimeoutException();
            }
            try{
                System.out.println(locatorone.isDisplayed());
                break;
            }
            catch (NoSuchElementException e){
                Thread.sleep(1000);
            }
        }
    }
    public static void chromeenabled(WebElement locatortwo) throws InterruptedException {

        for(int count = 0;;count++){
            if(count>=10){
                throw new TimeoutException();
            }
            try{
                System.out.println(locatortwo.isDisplayed());
                break;
            }
            catch (NoSuchElementException e){
                Thread.sleep(1000);
            }
        }
    }

}
