package automatizationlessons.firstTest;

import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.path.json.JsonPath;
import io.restassured.path.json.config.JsonPathConfig;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
public class TestApi {

    @Test
    public void token(){
        Map<String, String> map = new HashMap<>();
        map.put("email", "uqnnmqja@yomail.info");
        map.put("password", "TeSt@09111");
        map.put("organisationId", "a95cc30c-4f14-482c-9948-87006b0cf17f");

//                        body("{\"email\":\"uqnnmqja@yomail.info\",\"\r\n" +
//                                "               + \" \"organisationId\": \"a95cc30c-4f14-482c-9948-87006b0cf17f\",\"\r\n" +
//                                "               + \" \"lpassword\": TeSt@09111}").
//                        auth().preemptive().basic("uqnnmqja@yomail.info", "TeSt@09111")
//                .body("{\\\"organisationId\\\": \\\"a95cc30c-4f14-482c-9948-87006b0cf17f}")
//                .
        Response response =
                RestAssured.given().
                        body(map)
//                        auth().basic("uqnnmqja@yomail.info", "TeSt@09111")
//                .auth().preemptive().oauth2("a95cc30c-4f14-482c-9948-87006b0cf17f")
                .contentType(ContentType.JSON).

//        body("{\n" +
//        "\"email\": \"uqnnmqja@yomail.info\",\n" +
//        "\"organisationId\": \"a95cc30c-4f14-482c-9948-87006b0cf17f\",\n" +
//        "\"password\": \"TeSt@09111\"\n" +
//        "}").

                        when().
                        post("https://ca-kordamentha.andersenlab.com/api/account/checkPasswordSignIn")
                .then().statusCode(200)
                        .body("phoneNumber", equalTo("+61412457856"))
                .extract().response();

        response.getBody().print();
        String token = response.path("verificationCode").toString();
        System.out.println(token);

    }
}
