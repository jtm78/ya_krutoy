package automatizationlessons.firstTest.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserInitialization {

    static WebDriver driver;


    public static WebDriver getDriver(String browser){
        if(driver==null){
            switch (browser){
                case "chrome":
                    System.setProperty("webdriver.chrome.driver", "/home/anduser/Загрузки/beer/ChromeDriver");
                    driver = new ChromeDriver();
                    break;
                case "firefox":
                    System.setProperty("webdriver.gecko.driver", "/home/anduser/Загрузки/beer/geckodriver");
                    driver = new FirefoxDriver();
                    break;
            }
        }
        return driver;
    }
}
