package automatizationlessons.firstTest.core;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;

import java.util.concurrent.TimeUnit;

public class OptionsForBrowser extends BrowserInitialization {


    public static WebDriver getDriverConfiguration(String browser){
        getDriver(browser);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        return driver;
    }

    @AfterTest
    public static void quit(){
        driver.quit();
    }
}
