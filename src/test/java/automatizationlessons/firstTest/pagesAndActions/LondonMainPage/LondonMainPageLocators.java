package automatizationlessons.firstTest.pagesAndActions.LondonMainPage;

import org.openqa.selenium.By;

public class LondonMainPageLocators {
    private static By moreForLondon = By.xpath("//a[contains(@class, \"dropdown2__switcher\")]");
    //private static By moreForLondon = By.xpath("//a[@class=\"home-link home-link_blue_yes home-tabs__link home-tabs__more-switcher dropdown2__switcher\"]");
    private static By listForLondon = By.xpath("//div[@class=\"home-tabs__more\"]//div[@role=\"menuitem\"]");

    public static By getMoreForLondon(){
        return moreForLondon;
    }
    public static By getListForLondon(){
        return listForLondon;
    }
}
