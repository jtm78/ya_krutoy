package automatizationlessons.firstTest.pagesAndActions.LondonMainPage;

import automatizationlessons.firstTest.pagesAndActions.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;


/*Класс, когда Лондон выбран как город*/


public class LondonMainPageActions extends BaseTest {
    private static List<WebElement> londonList;

    public static void openMoreForLondon() {
        getWait().until(ExpectedConditions.elementToBeClickable(LondonMainPageLocators.getMoreForLondon()));
        getDriver().findElement(LondonMainPageLocators.getMoreForLondon()).click();
    }

    public static List<WebElement> londonList() {
        londonList = getDriver().findElements(LondonMainPageLocators.getListForLondon());
        return londonList;

    }
}
