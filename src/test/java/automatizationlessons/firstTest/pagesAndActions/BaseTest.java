package automatizationlessons.firstTest.pagesAndActions;

import automatizationlessons.firstTest.core.Link;
import automatizationlessons.firstTest.core.OptionsForBrowser;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseTest {
    private static WebDriver driver = OptionsForBrowser.getDriverConfiguration("chrome");
    private static WebDriverWait wait = new WebDriverWait(driver, 5);


    public static void openYandexLink(){
       driver.get(Link.getYandexlink());
    }

    public static WebDriver getDriver(){
        return driver;
    }
    public static WebDriverWait getWait(){
        return wait;
    }


}
