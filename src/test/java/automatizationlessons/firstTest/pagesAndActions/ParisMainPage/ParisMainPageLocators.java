package automatizationlessons.firstTest.pagesAndActions.ParisMainPage;

import org.openqa.selenium.By;

public class ParisMainPageLocators {
    private static By moreForParis = By.xpath("//a[contains(@class, \"dropdown2__switcher\")]");
    private static By listForParis = By.xpath("//div[@class=\"home-tabs__more\"]//div[@role=\"menuitem\"]");

    public static By getMoreForParis(){
        return moreForParis;
    }
    public static By getListForParis(){
        return listForParis;
    }
}
