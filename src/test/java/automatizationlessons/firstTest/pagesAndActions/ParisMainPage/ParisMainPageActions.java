package automatizationlessons.firstTest.pagesAndActions.ParisMainPage;

import automatizationlessons.firstTest.pagesAndActions.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;


/*Класс, когда Париж выбран как город*/


public class ParisMainPageActions extends BaseTest {

    private static List<WebElement> parisList;

    public static void openMoreForParis() {
        getWait().until(ExpectedConditions.elementToBeClickable(ParisMainPageLocators.getMoreForParis()));
        getDriver().findElement(ParisMainPageLocators.getMoreForParis()).click();
    }

    public static List<WebElement> parisList() {
        parisList = getDriver().findElements(ParisMainPageLocators.getListForParis());
        return parisList;


    }
}
