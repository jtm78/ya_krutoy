package automatizationlessons.firstTest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.equalTo;

public class DeleteMove {


    @Test
    public void justPost(){
        Response response = RestAssured.given().
                when().
                get("http://httpbin.org/get?a=1").
                then().statusCode(200)
                .body("args.a", equalTo("1")).extract().response();
        response.getBody().print();
    }
}
