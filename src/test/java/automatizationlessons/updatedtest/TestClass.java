package automatizationlessons.updatedtest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class TestClass {


    static WebDriver driver;


    public static WebDriver getDriver(String browser) {

        if (driver == null) {
            switch (browser) {
                case "chrome":
                    driver = new ChromeDriver();
                    break;

                case "firefox":
                    System.setProperty("webdriver.gecko.driver", "/home/anduser/Загрузки/beer/geckodriver");
                    driver = new FirefoxDriver();
                    break;

            }

        }
        return driver;
    }

    public static void getSize() {
        driver.manage().window().maximize();
    }
    public static void neyavnoeOjidanie(){
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    public static void getLink() {
        driver.get("https://www.bbc.com/");
    }

    public static void quit() {
        driver.quit();
    }

}
