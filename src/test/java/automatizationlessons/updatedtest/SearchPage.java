package automatizationlessons.updatedtest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchPage {

    private By searchField = By.xpath("//input[@id=\"orb-search-q\"]");
    private By searchSubmit = By.xpath("//button[@id=\"orb-search-button\"]");
    private By firstLink = By.xpath("//a[@href=\"https://www.bbc.co.uk/programmes/w172wyj6bx5r385\"]//ancestor::h1");


    private WebDriver driver;


    public SearchPage(WebDriver driver) {
        this.driver = driver;
    }


    public void findSearchField(String input) {
        driver.findElement(searchField).sendKeys(input);

    }

    public void sumbitButton() {
        driver.findElement(searchSubmit).submit();
    }

    public void findFirstLink() {
        driver.findElement(firstLink).submit();
    }

    public void isDisplayed(){
        System.out.println(driver.findElement(searchField).isDisplayed());
    }
    public void isEnabled(){
        System.out.println(driver.findElement(searchField).isEnabled());
    }
}
