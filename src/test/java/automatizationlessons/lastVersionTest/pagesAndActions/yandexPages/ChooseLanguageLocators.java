package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import org.openqa.selenium.By;

public class ChooseLanguageLocators {
    private  By chooseEnglishFromDropdown = By.xpath("//select[@class='select__control']");
    private  By openChooseEnlisghDropdown = By.xpath("//button[@type=\"button\"]");


    public  By getChooseEnglishFromDropdown() {
        return chooseEnglishFromDropdown;
        }

    public  By getOpenChooseEnlisghDropdown() {
        return openChooseEnlisghDropdown;
    }
}

