package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import automatizationlessons.lastVersionTest.wrapperClasses.*;
import io.qameta.allure.Step;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class YandexActionsCamerasPage extends Elements {

    @Step("Sort cameras by price and wait until page is downloaded")
    public  void sortByPrice()  {
        YandexActionsCamerasLocators yandexActionsCamerasLocators = new YandexActionsCamerasLocators();
        Button button = new Button();
        button.click(yandexActionsCamerasLocators.getSortByPriceButton());
        Elements elements = new Elements();
        elements.waitUntilPageIsLoaded();



    }
    @Step("Get list with sorted prices by Collections ")
    public  List<Integer> expectedList(){
        List<Integer> itemsSortedByCollections = getListWithSortedItemsByCite();
        Collections.sort(itemsSortedByCollections);
        return itemsSortedByCollections;
    }

    @Step("Get list with sorder prices by Yandex Page")
    public  List<Integer> getListWithSortedItemsByCite()  {
        YandexActionsCamerasLocators yandexActionsCamerasLocators = new YandexActionsCamerasLocators();
        RememberWebElementList rememberWebElementList = new RememberWebElementList();
        List<WebElement> sortedItems = rememberWebElementList.rememberList(yandexActionsCamerasLocators.getAllPricesInSortedOrder());
        System.out.println(sortedItems.size());
        List<String> textFromSortedItems = new ArrayList<>();
        for(int i=0; i<sortedItems.size(); i++){
            textFromSortedItems.add(sortedItems.get(i).getText());
        }
        List<String> textWithOnlyIntegerItems = new ArrayList<>();
        for(int i=0; i<textFromSortedItems.size(); i++){
            if(textFromSortedItems.get(i).contains(",")) {
                textWithOnlyIntegerItems.add(StringUtils.substring(textFromSortedItems.get(i), 0, -8));
            }
            else {
                textWithOnlyIntegerItems.add(StringUtils.substring(textFromSortedItems.get(i), 0, -5));
            }

        }
        System.out.println(textWithOnlyIntegerItems);
        List<Integer> prices = new ArrayList<>();
        for(String stringValue: textWithOnlyIntegerItems){
            try {

                prices.add(Integer.parseInt(stringValue));
            }
            catch (NumberFormatException e){
                System.out.println("Number isn't integer");
            }
        }
        return prices;

    }
}
