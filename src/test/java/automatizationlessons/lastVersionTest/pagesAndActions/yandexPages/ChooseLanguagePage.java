package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.ChooseLanguageLocators;
import automatizationlessons.lastVersionTest.wrapperClasses.Button;

public class ChooseLanguagePage {


    public void chooseLanguage(){
        ChooseLanguageLocators chooseLanguageLocators = new ChooseLanguageLocators();
        Button button = new Button();
        button.click(chooseLanguageLocators.getOpenChooseEnlisghDropdown());
        button.click(chooseLanguageLocators.getChooseEnglishFromDropdown());
    }
}
