package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import org.openqa.selenium.By;

public class YandexFridgesLocators {

    public  By getWidthForTo = By.xpath("//input[@id=\"15464317to\"]");
    public  By firstFieldFromDescription = By.xpath("//ul[@class=\"n-snippet-card2__desc n-snippet-card2__desc_type_list\"]//child::li[1]");


    public  By getGetWidthForTo() {
        return getWidthForTo;
    }

    public  By getFirstFieldFromDescription() {
        return firstFieldFromDescription;
    }
}
