package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import automatizationlessons.lastVersionTest.wrapperClasses.Button;
import automatizationlessons.lastVersionTest.wrapperClasses.MoveMouse;
import automatizationlessons.lastVersionTest.wrapperClasses.Text;

import java.util.ArrayList;
import java.util.List;

public class YandexMarketResultOfSearchPage {

    public  List<String> addTwoItemsIntoCompare(){
        MoveMouse moveMouse = new MoveMouse();
        YandexMarketResulfOfSearchLocators yandexMarketResulfOfSearchLocators = new YandexMarketResulfOfSearchLocators();
        moveMouse.moveToElement(yandexMarketResulfOfSearchLocators.getChooseFirstItemFromList(), yandexMarketResulfOfSearchLocators.getAddFirstItemIntoCompare());
        moveMouse.moveToElement(yandexMarketResulfOfSearchLocators.getChooseSecondItemFromList(), yandexMarketResulfOfSearchLocators.getAddSecondItemIntoCompare());
        Text text = new Text();
        List<String> listWithTwoFirstElements = new ArrayList<>();
        listWithTwoFirstElements.add(text.getTextFromLocator(yandexMarketResulfOfSearchLocators.getNameOfFirstElement()));
        listWithTwoFirstElements.add(text.getTextFromLocator(yandexMarketResulfOfSearchLocators.getNameOfSecondElement()));
        return listWithTwoFirstElements;

    }

    public  void openComparisons(){
        Button button = new Button();
        YandexMarketResulfOfSearchLocators yandexMarketResulfOfSearchLocators = new YandexMarketResulfOfSearchLocators();
        button.click(yandexMarketResulfOfSearchLocators.getComparisonsButton());
    }
}
