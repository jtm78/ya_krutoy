package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;


import automatizationlessons.lastVersionTest.wrapperClasses.Button;
import automatizationlessons.lastVersionTest.wrapperClasses.RememberWebElementList;
import automatizationlessons.lastVersionTest.wrapperClasses.Text;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;


import java.util.*;


/* Дефолтная страница яндекса*/


public class YandexMainPageActions {

    public  void clickOnLocation() {
        Button button = new Button();
        YandexMainPageLocators yandexMainPageLocators = new YandexMainPageLocators();
        button.click(yandexMainPageLocators.getLocation());
    }

    public  void openMoreDropdown() {
        Button button = new Button();
        YandexMainPageLocators yandexMainPageLocators = new YandexMainPageLocators();
        button.click(yandexMainPageLocators.getMoreDropdown());
    }

    public  List<String> rememberWebElementListToString() {
        RememberWebElementList rememberWebElementList = new RememberWebElementList();
        YandexMainPageLocators yandexMainPageLocators = new YandexMainPageLocators();
        List<WebElement> listone = rememberWebElementList.rememberList(yandexMainPageLocators.getMoreList());

        List<String> stringList = new ArrayList<>();
        for (int i = 0; i < listone.size(); i++) {
            stringList.add(listone.get(i).getText());
        }
        return stringList;
    }

//    public static List<String> setStringList(List<WebElement> list) {
//        List<String> stringList = new ArrayList<>();
//        for (int i = 0; i < list.size(); i++) {
//            stringList.add(list.get(i).getText());
//        }
//        return stringList;
//    }
    @Step("Click on Login button")
    public  void clickOnLoginButton(){
        Button button = new Button();
        YandexMainPageLocators yandexMainPageLocators = new YandexMainPageLocators();
        button.click(yandexMainPageLocators.getLoginIntoYourMail());
    }

    public  String getLogInText(){
        Text text = new Text();
        YandexMainPageLocators yandexMainPageLocators = new YandexMainPageLocators();
        return text.getTextFromLocator(yandexMainPageLocators.getLoginIntoYourMail());
    }

    public  void openChooseLanguagePage(){
        Button button = new Button();
        YandexMainPageLocators yandexMainPageLocators = new YandexMainPageLocators();
        button.click(yandexMainPageLocators.getCurrentLanguageLocator());
        button.click(yandexMainPageLocators.getMoreLanguagesFieldFromDropdown());
    }
}




