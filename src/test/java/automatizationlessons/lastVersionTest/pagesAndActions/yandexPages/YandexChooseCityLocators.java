package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import org.openqa.selenium.By;

public class YandexChooseCityLocators {

    private  By city = By.xpath("//input[@class=\"input__control input__input\"]");
    private  By london = By.xpath("//div[text() = \"Великобритания\"]");
    private  By paris = By.xpath("//div[text() = \"Франция\"]");


    public  By getCity() {
        return city;
    }


    public  By getLondon() {
        return london;
    }

    public  By getParis() {
        return paris;
    }

}
