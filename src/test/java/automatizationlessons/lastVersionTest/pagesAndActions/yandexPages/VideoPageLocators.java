package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import org.openqa.selenium.By;

public class VideoPageLocators {

    public  By videoIcon = By.xpath("//div[@class=\"service service_name_video\"]//a");


    public  By getVideoIcon() {
        return videoIcon;
    }
}
