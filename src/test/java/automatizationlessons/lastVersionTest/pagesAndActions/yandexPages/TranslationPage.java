package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import automatizationlessons.lastVersionTest.wrapperClasses.Button;
import automatizationlessons.lastVersionTest.wrapperClasses.GetUrl;
import automatizationlessons.lastVersionTest.wrapperClasses.SwitchToNextTab;
import automatizationlessons.lastVersionTest.wrapperClasses.SwitchToBeforeTab;

public class TranslationPage {

    public  void openTranslationPage(){
        TranslationPageLocators translationPageLocators = new TranslationPageLocators();
        Button button = new Button();
        button.click(translationPageLocators.getTranslationIcon());
    }

    public  String currentTranslationUrl(){
        SwitchToNextTab switchToNextTab = new SwitchToNextTab();
        switchToNextTab.changeTabTo();
        GetUrl getUrl = new GetUrl();
        return getUrl.getCurrentURL();
    }



}
