package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import org.openqa.selenium.By;

public class YandexNewsLocators {

    private  By newsIcon = By.xpath("//a[text()=\"Новости\"]");


    public  By getNewsIcon() {
        return newsIcon;
    }
}
