package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import automatizationlessons.lastVersionTest.wrapperClasses.Button;
import automatizationlessons.lastVersionTest.wrapperClasses.Text;

import java.util.ArrayList;
import java.util.List;

public class YandexComparisonsPage {



    public  List<String> nameOfTwoItemsFromComparisons(){
        List<String> listWithTwoItems = new ArrayList<>();
        Text text = new Text();
        YandexComparisonsLocators yandexComparisonsLocators = new YandexComparisonsLocators();
        listWithTwoItems.add(text.getTextFromLocator(yandexComparisonsLocators.getSecondItemName()));
        listWithTwoItems.add(text.getTextFromLocator(yandexComparisonsLocators.getFirstItemName()));
        return listWithTwoItems;
    }

    public  void deleteAllElements(){
        Button button = new Button();
        YandexComparisonsLocators yandexComparisonsLocators = new YandexComparisonsLocators();
        button.click(yandexComparisonsLocators.getDeleteAllElementsButton());
    }

    public  String getTextThatListIsEmpty(){
        Text text = new Text();
        YandexComparisonsLocators yandexComparisonsLocators = new YandexComparisonsLocators();
        return text.getTextFromLocator(yandexComparisonsLocators.getNoAnyItemsExistText());
    }
}
