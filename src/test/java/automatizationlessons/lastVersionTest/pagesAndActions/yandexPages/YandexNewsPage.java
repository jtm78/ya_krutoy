package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import automatizationlessons.lastVersionTest.wrapperClasses.Button;
import automatizationlessons.lastVersionTest.wrapperClasses.GetUrl;
import automatizationlessons.lastVersionTest.wrapperClasses.SwitchToNextTab;

public class YandexNewsPage {

    public  void openNewsPage(){
        Button button = new Button();
        YandexNewsLocators yandexNewsLocators = new YandexNewsLocators();
        button.click(yandexNewsLocators.getNewsIcon());
    }





    public  String getCurrentNewsUrl(){
        SwitchToNextTab switchToNextTab = new SwitchToNextTab();
        switchToNextTab.changeTabTo();
        return new GetUrl().getCurrentURL();
    }
}
