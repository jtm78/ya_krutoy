package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import automatizationlessons.lastVersionTest.wrapperClasses.Button;
import automatizationlessons.lastVersionTest.wrapperClasses.Elements;
import automatizationlessons.lastVersionTest.wrapperClasses.RememberWebElementList;
import automatizationlessons.lastVersionTest.wrapperClasses.Value;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class YandexFridgesPage {


    public  void setWidth(String value){
        Button button = new Button();
        YandexFridgesLocators yandexFridgesLocators = new YandexFridgesLocators();
        button.click(yandexFridgesLocators.getGetWidthForTo());

        Value value1 = new Value();
        value1.sendValue(yandexFridgesLocators.getGetWidthForTo(), value);

        new Elements().waitUntilPageIsLoaded();
    }


    public  Boolean listWithWidth(){
        RememberWebElementList rememberWebElementList = new RememberWebElementList();
        YandexFridgesLocators yandexFridgesLocators = new YandexFridgesLocators();
        List<WebElement> webElementList = rememberWebElementList.rememberList(yandexFridgesLocators.getFirstFieldFromDescription());
        List<String> listWithTextFromItems = new ArrayList<>();
        for (int i=0; i<webElementList.size(); i++){
            listWithTextFromItems.add(webElementList.get(i).getText());
        }
        List<String> listWithoutTextInTheStart = new ArrayList<>();
        for (int i=0; i<listWithTextFromItems.size(); i++){
            listWithoutTextInTheStart.add(listWithTextFromItems.get(i).substring(listWithTextFromItems.get(i).indexOf(" ")+1));
        }
        List<String> listWithStringWidth = new ArrayList<>();
        for (int i=0; i<listWithoutTextInTheStart.size(); i++){
            listWithStringWidth.add(StringUtils.substring(listWithoutTextInTheStart.get(i), 0, 2));
        }
//        listWithStringWidth.remove(4);
//        sou
        List<Integer> listWithIntegerWidth = new ArrayList<>();
        System.out.println(listWithStringWidth);
        for(String list : listWithStringWidth){
            try {
                listWithIntegerWidth.add(Integer.parseInt(list));
            }
            catch (NumberFormatException e){
                continue;
            }
//            listWithIntegerWidth.add(Integer.parseInt(list));
        }
        for (Integer list : listWithIntegerWidth){
            if (list>50){
                return false;
            }
        }
        return true;
    }
}
