package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import org.openqa.selenium.By;

public class YandexMarketLocators {

    private By marketIcon = By.xpath("//a[text()=\"Маркет\"]");
    private By searchField = By.xpath("//input[@id=\"header-search\"]");
    private By findButton = By.xpath("//button[@type=\"submit\"]");
    private By openAllCategories = By.xpath("//div[@class=\"n-w-tab__control-hamburger\"]");
    private By actionsCameraLink = By.xpath("//a[@title=\"Экшн-камеры\"]");
    private By bitovayaElectronikaLink = By.xpath("//div[contains(@class, \"n-w-tab n-w-tab_interaction\")]//a[contains(@href, \"catalog--bytovaia-tekhnika\")]");
    private By fridges = By.xpath("//a[@title=\"Холодильники\"]");


    public By getMarketIcon() {
        return marketIcon;
    }


    public By getSearchField() {
        return searchField;
    }

    public By getFindButton() {
        return findButton;
    }


    public By getOpenAllCategories() {
        return openAllCategories;
    }

    public By getActionsCameraLink() {
        return actionsCameraLink;
    }

    public By getBitovayaElectronikaLink() {
        return bitovayaElectronikaLink;
    }

    public By getFridges() {
        return fridges;
    }
}
