package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import automatizationlessons.lastVersionTest.wrapperClasses.*;


public class YandexMusicPage {

    public  void openMusicPage(){
        Button button = new Button();
        YandexMusicLocators yandexMusicLocators = new YandexMusicLocators();
        button.click(yandexMusicLocators.getMusicIcon());
    }

    public  String currentMusicUrl(){
        SwitchToNextTab switchToNextTab = new SwitchToNextTab();
        switchToNextTab.changeTabTo();
        return new GetUrl().getCurrentURL();
    }

    public  void toLastTab(){
        SwitchToBeforeTab switchToBeforeTab = new SwitchToBeforeTab();
        switchToBeforeTab.switchToTabNumberOne();
    }

    public  void findMusic(String value){
        Button button = new Button();
        YandexMusicLocators yandexMusicLocators = new YandexMusicLocators();
        button.click(yandexMusicLocators.getSearchField());
        Value value1 = new Value();
        value1.sendValue(yandexMusicLocators.getSearchField(), value);
    }

    public  void chooseMetallicaFromDropdown(){
        Button button = new Button();
        YandexMusicLocators yandexMusicLocators = new YandexMusicLocators();
        button.click(yandexMusicLocators.getChooseMetalicaFromSearch());
    }
    public  void chooseBeyonceFromDropdown(){
        new Elements().waitUntilPageIsLoaded();
        Button button = new Button();
        YandexMusicLocators yandexMusicLocators = new YandexMusicLocators();
        button.click(yandexMusicLocators.getChooseBeyonceFromSearch());
    }

}
