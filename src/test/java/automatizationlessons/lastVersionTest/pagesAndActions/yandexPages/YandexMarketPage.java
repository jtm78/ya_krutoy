package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import automatizationlessons.lastVersionTest.wrapperClasses.*;
import io.qameta.allure.Step;


public class YandexMarketPage {
    @Step("Open market page")
    public  void openMarketPage(){
        Button button = new Button();
        YandexMarketLocators yandexMarketLocators = new YandexMarketLocators();
        button.click(yandexMarketLocators.getMarketIcon());
    }

    public  String currentMarketUrl(){
        SwitchToNextTab switchToNextTab = new SwitchToNextTab();
        switchToNextTab.changeTabTo();
        return new GetUrl().getCurrentURL();
    }

    public  void findItem(String request){
        Value value = new Value();
        YandexMarketLocators yandexMarketLocators = new YandexMarketLocators();
        value.sendValue(yandexMarketLocators.getSearchField(), request);
        Button button = new Button();
        button.click(yandexMarketLocators.getFindButton());
    }
    @Step("Open Cameras from catalog")
    public  void openActionsCameraFromElectronics(){
        Button button = new Button();
        YandexMarketLocators yandexMarketLocators = new YandexMarketLocators();
        button.click(yandexMarketLocators.getOpenAllCategories());
        button.click(yandexMarketLocators.getActionsCameraLink());
    }

    public  void openFridgesFromCategories(){
        Button button = new Button();
        YandexMarketLocators yandexMarketLocators = new YandexMarketLocators();
        button.click(yandexMarketLocators.getOpenAllCategories());
        MoveMouse moveMouse = new MoveMouse();
        moveMouse.moveToElement(yandexMarketLocators.getBitovayaElectronikaLink(), yandexMarketLocators.getFridges());
    }


}
