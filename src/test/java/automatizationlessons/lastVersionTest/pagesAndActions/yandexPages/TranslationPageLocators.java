package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import org.openqa.selenium.By;

public class TranslationPageLocators {


    private  By translationIcon = By.xpath("//a[text()=\"Переводчик\"]");


    public  By getTranslationIcon() {
        return translationIcon;
    }
}
