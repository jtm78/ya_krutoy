package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import automatizationlessons.lastVersionTest.wrapperClasses.*;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class YandexMusicResultOfSearchPage {
    public  String getNameOfComposer(){
        Text text = new Text();
        YandexMusicResultOfSearchLocators yandexMusicResultOfSearchLocators = new YandexMusicResultOfSearchLocators();
        return text.getTextFromLocator(yandexMusicResultOfSearchLocators.getNameOfComposer());
    }

    public  void playFirstSong(){
        new Elements().waitUntilPageIsLoaded();
//        MoveMouse.moveToElement(YandexMusicResultOfSearchLocators.getPlayFirstSongForBeyonce(), YandexMusicResultOfSearchLocators.getPlayButton());
        TestClassWithScrips testClassWithScrips = new TestClassWithScrips();
        testClassWithScrips.runFirstSong();
    }

    public  Boolean songIsActive(){
        Windows windows = new Windows();
        YandexMusicResultOfSearchLocators yandexMusicResultOfSearchLocators = new YandexMusicResultOfSearchLocators();
        return windows.isWindowsDisplays(yandexMusicResultOfSearchLocators.getSongIsActiveIcon());
    }
    public  Boolean songIsNotActive(){
        Windows windows = new Windows();
        YandexMusicResultOfSearchLocators yandexMusicResultOfSearchLocators = new YandexMusicResultOfSearchLocators();
        return windows.isWindowsDisplays(yandexMusicResultOfSearchLocators.getSongIsNotActiveIcon());
    }

    public  Boolean checkWhetherAlbumComposerIsTheSameAsRequested(String composer){
        RememberWebElementList rememberWebElementList = new RememberWebElementList();
        YandexMusicResultOfSearchLocators yandexMusicResultOfSearchLocators = new YandexMusicResultOfSearchLocators();
        List<WebElement> listWithNameOfComposer = rememberWebElementList.rememberList(yandexMusicResultOfSearchLocators.getAlbumsComposer());
        List<String> listWithTextFromWebElements = new ArrayList<>();
        for(int i=0; i<listWithNameOfComposer.size(); i++){
            listWithTextFromWebElements.add(listWithNameOfComposer.get(i).getText());
        }
        System.out.println(listWithTextFromWebElements);
        for(String list : listWithTextFromWebElements){
            if (!list.equals(composer)){
                return false;
            }
        }
        return true;
    }
}
