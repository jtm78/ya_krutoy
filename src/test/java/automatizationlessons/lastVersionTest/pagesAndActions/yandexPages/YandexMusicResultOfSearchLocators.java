package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import org.openqa.selenium.By;

public class YandexMusicResultOfSearchLocators {
    private  By nameOfComposer = By.xpath("//h1[@class=\"page-artist__title typo-h1 typo-h1_big\"]");
    private  By playFirstSongForBeyonce = By.xpath("//div[@class=\"page-artist__tracks_top\"]/child::div[1]");
    private  By playButton = By.xpath("//div[@class=\"page-artist__tracks_top\"]/child::div[1]/child::div[1]//button");
    // РАБОЧИЙ - //div[@class="page-artist__tracks_top"]/child::div[1]/child::div[1]//button
    //div[@class="page-artist__tracks_top"]/child::div[1]/child::div[1]//span[@class="d-icon deco-icon d-icon_play-small  "]
    //div[@class="d-track__play d-track__hover"]//child::button[@data-idx="631024"]
    private  By albumsComposer = By.xpath("//a[@title=\"Metallica\"]");
    private  By songIsActiveIcon = By.xpath("//div[@class=\"player-controls__btn deco-player-controls__button player-controls__btn_play player-controls__btn_pause\"]");
    private  By songIsNotActiveIcon = By.xpath("//div[@class=\"player-controls__btn deco-player-controls__button player-controls__btn_play\"]");


    public  By getNameOfComposer() {
        return nameOfComposer;
    }

    public  By getAlbumsComposer() {
        return albumsComposer;
    }

    public  By getPlayFirstSongForBeyonce() {
        return playFirstSongForBeyonce;
    }

    public  By getPlayButton() {
        return playButton;
    }

    public  By getSongIsActiveIcon() {
        return songIsActiveIcon;
    }

    public  By getSongIsNotActiveIcon() {
        return songIsNotActiveIcon;
    }
}
