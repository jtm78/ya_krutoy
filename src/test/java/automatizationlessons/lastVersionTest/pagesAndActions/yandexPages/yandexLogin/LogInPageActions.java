package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.yandexLogin;

import automatizationlessons.lastVersionTest.wrapperClasses.Button;
import automatizationlessons.lastVersionTest.wrapperClasses.Text;
import automatizationlessons.lastVersionTest.wrapperClasses.Value;
import automatizationlessons.lastVersionTest.wrapperClasses.Windows;
import io.qameta.allure.Step;

public class LogInPageActions {

    @Step("Input username: {0}")
    public  void inputLogin(String email) {
        Value value = new Value();
        LogInPageLocators logInPageLocators = new LogInPageLocators();
        value.sendValue(logInPageLocators.getLoginField(), email);
        Button button = new Button();
        button.click(logInPageLocators.getSubmitButton());
    }
    @Step("Input password: {0}")
    public  void inputPassword(String password) {
        Value value = new Value();
        LogInPageLocators logInPageLocators = new LogInPageLocators();
        value.sendValue(logInPageLocators.getPasswordField(), password);
        Button button = new Button();
        button.click(logInPageLocators.getSubmitButton());
    }


    public  String getTextFromInvalidLoginWindow() {
        Text text = new Text();
        LogInPageLocators logInPageLocators = new LogInPageLocators();
        return text.getTextFromLocator(logInPageLocators.getInvalidLoginField());
    }

    public  Boolean whetherIncorrectPasswordWindowDisplays() {
        Windows windows = new Windows();
        LogInPageLocators logInPageLocators = new LogInPageLocators();
        return windows.windowDisplays(logInPageLocators.getIncorrectPasswordWindow());
    }


}
