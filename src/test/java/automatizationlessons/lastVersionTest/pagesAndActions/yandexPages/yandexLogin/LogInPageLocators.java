package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.yandexLogin;

import org.openqa.selenium.By;

public class LogInPageLocators {

    private  By loginField = By.xpath("//input[@id=\"passp-field-login\"]");
    private  By passwordField = By.xpath("//input[@type=\"password\"]");
    private  By submitButton = By.xpath("//button[@type=\"submit\"]");
    private  By incorrectPasswordWindow = By.xpath("//div[@class=\"passp-form-field__error\"]");
    private  By invalidLoginField = By.xpath("//div[contains(@class, \"passp-form-field__error\")]");

    public  By getPasswordField() {
        return passwordField;
    }

    public  By getSubmitButton() {
        return submitButton;
    }

    public  By getLoginField() {
        return loginField;
    }

    public  By getIncorrectPasswordWindow() {
        return incorrectPasswordWindow;
    }

    public  By getInvalidLoginField() {
        return invalidLoginField;
    }

}
