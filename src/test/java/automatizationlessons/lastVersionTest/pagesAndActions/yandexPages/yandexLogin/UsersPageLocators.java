package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.yandexLogin;

import org.openqa.selenium.By;

public class UsersPageLocators {


    private  By usersLogin = By.xpath("//span[@id=\"recipient-1\"]");
    private  By clientNameField = By.xpath("//div[@class=\"mail-User-Name\"]");
    private  By logOutButton = By.xpath("//a[contains (@data-metric, \"Sign out\")]");



    public  By getUsersLogin() {
        return usersLogin;
    }

    public  By getClientNameField() {
        return clientNameField;
    }

    public  By getLogOutButton() {
        return logOutButton;
    }


}
