package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.yandexLogin;

import automatizationlessons.lastVersionTest.wrapperClasses.Button;
import automatizationlessons.lastVersionTest.wrapperClasses.Text;

public class UsersPage {


    public  String getClientName(){
        Text text = new Text();
        UsersPageLocators usersPageLocators = new UsersPageLocators();
        return text.getTextFromLocator(usersPageLocators.getClientNameField());
    }

    public  void logOut(){
        Button button = new Button();
        UsersPageLocators usersPageLocators = new UsersPageLocators();
        button.click(usersPageLocators.getUsersLogin());
        button.click(usersPageLocators.getLogOutButton());
    }


}
