package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import org.openqa.selenium.By;

public class YandexActionsCamerasLocators {

    private  By sortByPriceButton = By.xpath("//a[contains(., \"по цене\")]");

    private  By allPricesInSortedOrder = By.xpath("//div[@class=\"price\"]");
//    private static By firstElement = By.xpath("//div[@class=\"n-snippet-list n-snippet-list_type_grid snippet-list_size_3 metrika b-zone b-spy-init b-spy-events i-bem metrika_js_inited snippet-list_js_inited b-spy-init_js_inited b-zone_js_inited\"]/child::div[1]");
//
//    public static By getFirstElement() {
//        return firstElement;
//    }

    public  By getSortByPriceButton(){
        return sortByPriceButton;
    }


    public  By getAllPricesInSortedOrder() {
        return allPricesInSortedOrder;
    }


}
