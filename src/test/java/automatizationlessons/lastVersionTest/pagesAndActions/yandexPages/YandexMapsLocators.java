package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import org.openqa.selenium.By;

public class YandexMapsLocators {

    private  By mapsIcon = By.xpath("//div[@class=\"service service_name_maps\"]/a");


    public  By getMapsIcon() {
        return mapsIcon;
    }
}
