package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;


import automatizationlessons.lastVersionTest.wrapperClasses.Button;
import automatizationlessons.lastVersionTest.wrapperClasses.Value;



/*Класс для выбора города*/


public class YandexChooseCityActions {

    public  void setCityLondon(String london) {
        Value value = new Value();
        YandexChooseCityLocators yandexChooseCityLocators = new YandexChooseCityLocators();
        value.sendValue(yandexChooseCityLocators.getCity(), london);
        Button button = new Button();
        button.click(yandexChooseCityLocators.getLondon());

    }

    public  void setCityParis(String paris) {
        Value value = new Value();
        YandexChooseCityLocators yandexChooseCityLocators = new YandexChooseCityLocators();
        value.sendValue(yandexChooseCityLocators.getCity(), paris);
        Button button = new Button();
        button.click(yandexChooseCityLocators.getParis());
    }
}
