package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import org.openqa.selenium.By;

public class YandexMainPageLocators {
    private  By location = By.xpath("//a[@class = \"home-link geolink link_geosuggest_yes home-link_black_yes\"]");
    private  By moreDropdown = By.xpath("//a[@data-statlog=\"tabs.more\"]");
    private  By moreList = By.xpath("//div[contains(@class,\"home-tabs__more\") and @role=\"menuitem\"]");
    private  By loginIntoYourMailButton = By.xpath("//a[contains (@class, \"button desk-notif-card__login-enter-expanded \")]");
    private  By currentLanguageLocator = By.xpath("//div[@class=\"col headline__bar-item b-langs\"]");
    private  By moreLanguagesFieldFromDropdown = By.xpath("//a[contains(@data-statlog, \"more\")]//span[@class=\"menu__text\"]");

    public  By getLocation() {
        return location;
    }


    public  By getMoreDropdown() {
        return moreDropdown;
    }

    public  By getMoreList() {
        return moreList;
    }


    public  By getLoginIntoYourMail(){
        return loginIntoYourMailButton;
    }

    public  By getCurrentLanguageLocator() {
        return currentLanguageLocator;
    }

    public  By getMoreLanguagesFieldFromDropdown() {
        return moreLanguagesFieldFromDropdown;
    }
}
