package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import automatizationlessons.lastVersionTest.wrapperClasses.Button;
import automatizationlessons.lastVersionTest.wrapperClasses.GetUrl;
import automatizationlessons.lastVersionTest.wrapperClasses.SwitchToNextTab;

public class VideoPage {


    public  void openVideoPage(){
        VideoPageLocators videoPageLocators = new VideoPageLocators();
        Button button = new Button();
        button.click(videoPageLocators.getVideoIcon());
    }


//    public static String videoCurrentUrl(){
//        return GetUrl.getCurrentURL();
//    }



    public  String getCurrentVideoUrl(){
        SwitchToNextTab switchToNextTab = new SwitchToNextTab();
        switchToNextTab.changeTabTo();
        GetUrl getUrl = new GetUrl();
        return getUrl.getCurrentURL();
    }
}
