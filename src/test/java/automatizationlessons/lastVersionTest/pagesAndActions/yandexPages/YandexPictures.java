package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import automatizationlessons.lastVersionTest.wrapperClasses.Button;
import automatizationlessons.lastVersionTest.wrapperClasses.GetUrl;
import automatizationlessons.lastVersionTest.wrapperClasses.SwitchToNextTab;

public class YandexPictures {


    public  void openImagesPage(){
        Button button = new Button();
        PicturesLocators picturesLocators = new PicturesLocators();
        button.click(picturesLocators.getClickonImages());
    }


    public  String imagesCurrentUrl(){
        return new GetUrl().getCurrentURL();
    }



    public  void switchTabToTheNext(){
        SwitchToNextTab switchToNextTab = new SwitchToNextTab();
        switchToNextTab.changeTabTo();
    }


}
