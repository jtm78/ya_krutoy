package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import org.openqa.selenium.By;

public class YandexComparisonsLocators {

    private  By firstItemName = By.xpath("//div[contains(@class, \"n-compare-content__line\")]/child::div[1]//a[contains(@class, \"head__name\")]");
    private  By secondItemName = By.xpath("//div[contains(@class, \"n-compare-content__line\")]/child::div[2]//a[contains(@class, \"head__name\")]");
    private  By deleteAllElementsButton = By.xpath("//div[@class=\"n-compare-toolbar__action\"]");
    private  By noAnyItemsExistText = By.xpath("//div[@class=\"title title_size_18\"]");


    public  By getFirstItemName() {
        return firstItemName;
    }

    public  By getSecondItemName() {
        return secondItemName;
    }

    public  By getDeleteAllElementsButton() {
        return deleteAllElementsButton;
    }

    public  By getNoAnyItemsExistText() {
        return noAnyItemsExistText;
    }
}
