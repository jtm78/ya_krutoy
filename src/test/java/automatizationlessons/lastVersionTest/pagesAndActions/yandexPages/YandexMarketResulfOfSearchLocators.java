package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import org.openqa.selenium.By;

public class YandexMarketResulfOfSearchLocators {
    private  By chooseFirstItemFromList = By.xpath("//div[contains(@class, \"n-snippet-list\")]/child::div[1]");
    private  By addFirstItemIntoCompare = By.xpath("//div[contains(@class, \"n-snippet-list\")]/child::div[1]//div[contains(@class, \"n-product-toolbar__item \")]");
    private  By nameOfFirstElement = By.xpath("//div[contains(@class, \"n-snippet-list\")]/child::div[1]//div[@class=\"n-snippet-cell2__title\"]//a");
    private  By chooseSecondItemFromList = By.xpath("//div[contains(@class, \"n-snippet-list\")]/child::div[2]");
    private  By addSecondItemIntoCompare = By.xpath("//div[contains(@class, \"n-snippet-list\")]/child::div[2]//div[contains(@class, \"n-product-toolbar__item \")]");
   private By comparisonsButton = By.xpath("//a[@class=\"button button_size_m button_theme_normal i-bem button_js_inited\"]");
    private  By nameOfSecondElement = By.xpath("//div[contains(@class, \"n-snippet-list\")]/child::div[2]//div[@class=\"n-snippet-cell2__title\"]//a");
//    private  By comparisonsButton = By.xpath("//span[@data-title=\"Сравнение\"]");


    public  By getChooseFirstItemFromList() {
        return chooseFirstItemFromList;
    }

    public  By getAddFirstItemIntoCompare() {
        return addFirstItemIntoCompare;
    }

    public  By getNameOfFirstElement() {
        return nameOfFirstElement;
    }

    public  By getChooseSecondItemFromList() {
        return chooseSecondItemFromList;
    }

    public  By getAddSecondItemIntoCompare() {
        return addSecondItemIntoCompare;
    }

    public  By getNameOfSecondElement() {
        return nameOfSecondElement;
    }

    public  By getComparisonsButton() {
        return comparisonsButton;
    }
}
