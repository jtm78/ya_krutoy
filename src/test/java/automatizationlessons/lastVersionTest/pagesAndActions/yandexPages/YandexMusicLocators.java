package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import org.openqa.selenium.By;

public class YandexMusicLocators {

    private  By musicIcon = By.xpath("//a[text()=\"Музыка\"]");
    private  By searchField = By.xpath("//div[contains(@class, \"d-input deco-input-wrapper d-input_size\")]//input");
    private  By chooseMetalicaFromSearch = By.xpath("//div[contains(@class, \"d-suggest__item-artist\")]//a[@href=\"/artist/3121\"]");
    private  By chooseBeyonceFromSearch = By.xpath("//div[contains(@class, \"d-suggest__item-artist\")]//a[@href=\"/artist/27995\"]");



    public  By getMusicIcon() {
        return musicIcon;
    }

    public  By getSearchField() {
        return searchField;
    }

    public  By getChooseMetalicaFromSearch() {
        return chooseMetalicaFromSearch;
    }

    public  By getChooseBeyonceFromSearch() {
        return chooseBeyonceFromSearch;
    }
}
