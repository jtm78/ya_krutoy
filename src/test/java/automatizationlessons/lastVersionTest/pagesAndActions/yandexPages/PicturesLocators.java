package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import org.openqa.selenium.By;

public class PicturesLocators {

    private  By clickonImages = By.xpath("//a[@data-id=\"images\"]");


    public  By getClickonImages() {
        return clickonImages;
    }
}
