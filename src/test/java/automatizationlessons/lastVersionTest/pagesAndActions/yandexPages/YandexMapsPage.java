package automatizationlessons.lastVersionTest.pagesAndActions.yandexPages;

import automatizationlessons.lastVersionTest.wrapperClasses.Button;
import automatizationlessons.lastVersionTest.wrapperClasses.GetUrl;
import automatizationlessons.lastVersionTest.wrapperClasses.SwitchToNextTab;
import automatizationlessons.lastVersionTest.wrapperClasses.SwitchToBeforeTab;

public class YandexMapsPage {


    public  void openMapPage(){
        Button button = new Button();
        YandexMapsLocators yandexMapsLocators = new YandexMapsLocators();
        button.click(yandexMapsLocators.getMapsIcon());
    }

    public  String currentMapUrl(){
        SwitchToNextTab switchToNextTab = new SwitchToNextTab();
        switchToNextTab.changeTabTo();
        return new GetUrl().getCurrentURL();
    }


}
