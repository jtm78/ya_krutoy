package automatizationlessons.lastVersionTest.wrapperClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MoveMouse extends Elements {


    public  void moveToElement(By whereToMove, By whereToClick){
        WebElement element = driver.findElement(whereToMove);
        Actions action = new Actions(driver);
        action.moveToElement(element).build().perform();
        element.findElement(whereToClick).click();


    }
}
