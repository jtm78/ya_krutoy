package automatizationlessons.lastVersionTest.wrapperClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

public class Windows extends Elements {

    public  Boolean  windowDisplays(By locator){
        return waitUntilClickable(locator).isDisplayed();
    }

    public  Boolean windownEnabled(By locator){
        return waitUntilClickable(locator).isEnabled();
    }

    public  Boolean isWindowsDisplays(By locator){
        try {
            return driver.findElement(locator).isDisplayed();
        }
        catch (NoSuchElementException e){
         return false;
        }

    }



}
