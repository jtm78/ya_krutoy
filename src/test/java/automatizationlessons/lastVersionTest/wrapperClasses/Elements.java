package automatizationlessons.lastVersionTest.wrapperClasses;

import automatizationlessons.lastVersionTest.core.BrauserInitialization;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

public class Elements {

    protected  WebDriver driver = BrauserInitialization.getDriver();
    private int TIME_OUT = 7;

    public  WebElement getWebElement(ExpectedCondition<WebElement> webElementExpectedConditions) {
        WebDriverWait wait = new WebDriverWait(driver, TIME_OUT);
        return wait.until(webElementExpectedConditions);
    }

    public  WebElement waitUntilClickable(By locator) {
        return getWebElement(ExpectedConditions.elementToBeClickable(locator));
    }

    public  List<WebElement> visible(By locatorr){
        List<WebElement> dynamicElement = (new WebDriverWait(driver, TIME_OUT))
                .until(ExpectedConditions.presenceOfAllElementsLocatedBy(locatorr));
        return dynamicElement;
    }
//   public static void test(){
//       FluentWait<WebDriver> fluentWait = new FluentWait<>(driver)
//               .withTimeout(30, TimeUnit.SECONDS)
//               .pollingEvery(200, TimeUnit.MILLISECONDS)
//               .ignoring(NoSuchElementException.class);
//   }
//    public static Boolean apply() {
//        WebElement button = driver.findElement(By.xpath("//div[@class=\"i-bem n-pager-more n-pager-more_js_inited\"]"));
//        String disabled = button.getAttribute("none");
//        return disabled == null;
//    }




    public  void waitUntilPageIsLoaded(){
        try {
             new WebDriverWait(driver, 5).until(ExpectedConditions.attributeContains(By.xpath("//a[contains(text(), \"по цене\")]"),"padding-right:", "15px"));
        }
        catch (TimeoutException e){

        }

    }






}
