package automatizationlessons.lastVersionTest.wrapperClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Text extends Elements {

    public  String getTextFromLocator(By locator){
        return waitUntilClickable(locator).getText();
    }
}
