package automatizationlessons.lastVersionTest.wrapperClasses;

import automatizationlessons.lastVersionTest.wrapperClasses.Elements;

import java.util.ArrayList;

public class SwitchToNextTab extends Elements {

    public  void changeTabTo(){

        for(String winHandle : driver.getWindowHandles())   // Switch to new window opened.
        {
            driver.switchTo().window(winHandle);
        }

    }
}
