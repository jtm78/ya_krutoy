package automatizationlessons.lastVersionTest.wrapperClasses;

import automatizationlessons.lastVersionTest.core.BrauserInitialization;

public class SetLink extends Elements {


    public static void openLink(String link) {
        BrauserInitialization.getDriver().get(link);

    }

}
