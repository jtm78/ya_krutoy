package automatizationlessons.lastVersionTest.wrapperClasses;

import java.util.ArrayList;

public class SwitchToBeforeTab extends Elements {

    public  void switchToTabNumberOne(){
        ArrayList<String> tabs2 = new ArrayList<> (driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
    }
}
