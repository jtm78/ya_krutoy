package automatizationlessons.lastVersionTest.wrapperClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Button extends Elements {

    public  void click(By locator) {
        waitUntilClickable(locator).click();

    }

    public  void submit(By locator) {
        waitUntilClickable(locator).submit();

    }

    public  void test(By locator){
        WebElement catalogTabElements = driver.findElement(locator);
        Actions actions = new Actions(driver);
        actions.moveToElement(catalogTabElements).build().perform();
        catalogTabElements.findElements(By.cssSelector("option")).get(4).click();
    }

    public  void clickTest(By locator){
        driver.findElement(locator).click();
    }

}
