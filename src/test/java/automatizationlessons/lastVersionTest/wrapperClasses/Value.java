package automatizationlessons.lastVersionTest.wrapperClasses;

import org.openqa.selenium.By;

public class Value extends Elements {

    public  void sendValue(By locator, String text) {
        waitUntilClickable(locator).clear();
        waitUntilClickable(locator).sendKeys(text);
    }
}
