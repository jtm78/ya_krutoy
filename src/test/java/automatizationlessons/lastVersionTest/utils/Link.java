package automatizationlessons.lastVersionTest.utils;


public enum Link {
    YANDEX_URL("https://yandex.by/"),
    BBC_URL("https://www.bbc.com/"),
    ANDERSENLAB("https://ca-kordamentha.andersenlab.com/corona/welcome/30_may");


    private String link;

    Link(String link){
        this.link=link;
    }

    public String getLink(){
        return link;
    }

}
