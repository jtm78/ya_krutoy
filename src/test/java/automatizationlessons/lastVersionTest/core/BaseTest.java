package automatizationlessons.lastVersionTest.core;


import automatizationlessons.lastVersionTest.core.BrauserInitialization;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;


public class BaseTest {



    private  String browser = "chrome";



    @BeforeMethod
    public  void create() {
        BrauserInitialization.createDriver(browser);
    }


    @AfterMethod
    public  void quit() {
        if (BrauserInitialization.getBrauserInitialization() != null) {
            BrauserInitialization.getDriver().quit();
            BrauserInitialization.brauserInitialization=null;
        }

    }


}
