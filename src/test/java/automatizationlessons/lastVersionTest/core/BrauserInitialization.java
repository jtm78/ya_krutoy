package automatizationlessons.lastVersionTest.core;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class BrauserInitialization extends OptionsForBrowser {

    private static WebDriver driver;
    protected static BrauserInitialization brauserInitialization;

    private BrauserInitialization(String browser){
        switch (browser) {
            case "chrome":

//                WebDriverManager.chromedriver().version("2.41").setup();
                    System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver");
                driver = new ChromeDriver(chromeOptions());
                break;

            case "firefox":
                System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/drivers/geckodriver");
                driver = new FirefoxDriver(firefoxOptions());
                break;

        }
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    public static BrauserInitialization createDriver(String browser) {

        if (brauserInitialization == null) {
            brauserInitialization = new BrauserInitialization(browser);

        }
        return brauserInitialization;
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static BrauserInitialization getBrauserInitialization() {
        return brauserInitialization;
    }
}
