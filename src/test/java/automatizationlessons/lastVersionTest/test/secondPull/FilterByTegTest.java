package automatizationlessons.lastVersionTest.test.secondPull;

import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexFridgesPage;
import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexMarketPage;
import automatizationlessons.lastVersionTest.core.BaseTest;
import automatizationlessons.lastVersionTest.utils.Link;
import automatizationlessons.lastVersionTest.wrapperClasses.SetLink;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FilterByTegTest extends BaseTest {


    @Test
    public void filterByTeg() {
        SetLink.openLink(Link.YANDEX_URL.getLink());
        YandexMarketPage yandexMarketPage = new YandexMarketPage();
        yandexMarketPage.openMarketPage();
        yandexMarketPage.openFridgesFromCategories();
        YandexFridgesPage yandexFridgesPage = new YandexFridgesPage();
        yandexFridgesPage.setWidth("50");
        boolean actualResult = yandexFridgesPage.listWithWidth();
        Assert.assertEquals(actualResult, true, "Ширина больше 50");
    }
}
