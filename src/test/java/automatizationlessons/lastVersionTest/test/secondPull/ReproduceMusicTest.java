package automatizationlessons.lastVersionTest.test.secondPull;

import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexMusicPage;
import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexMusicResultOfSearchPage;
import automatizationlessons.lastVersionTest.core.BaseTest;
import automatizationlessons.lastVersionTest.utils.Link;
import automatizationlessons.lastVersionTest.wrapperClasses.SetLink;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ReproduceMusicTest extends BaseTest {

    @Test
    public void reproduceBeyonceMusic(){
        SetLink.openLink(Link.YANDEX_URL.getLink());
        YandexMusicPage yandexMusicPage = new YandexMusicPage();
        yandexMusicPage.openMusicPage();
        yandexMusicPage.findMusic("beyo");
        yandexMusicPage.chooseBeyonceFromDropdown();
        YandexMusicResultOfSearchPage yandexMusicResultOfSearchPage = new YandexMusicResultOfSearchPage();
        yandexMusicResultOfSearchPage.playFirstSong();
        boolean musicIsActive = yandexMusicResultOfSearchPage.songIsActive();
        System.out.println(musicIsActive);
        Assert.assertEquals(musicIsActive, true, "Музыка не играет");
        yandexMusicResultOfSearchPage.playFirstSong();
        boolean musicIsNotActive = yandexMusicResultOfSearchPage.songIsNotActive();
        System.out.println(musicIsNotActive);
        Assert.assertEquals(musicIsNotActive, true, "Музыка воспроизведена");




    }
}
