package automatizationlessons.lastVersionTest.test.secondPull;

import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexMusicPage;
import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexMusicResultOfSearchPage;
import automatizationlessons.lastVersionTest.core.BaseTest;
import automatizationlessons.lastVersionTest.utils.Link;
import automatizationlessons.lastVersionTest.wrapperClasses.SetLink;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MusicTest extends BaseTest {

    @Test
    public void findMetalic(){
        SetLink.openLink(Link.YANDEX_URL.getLink());
        YandexMusicPage yandexMusicPage =new YandexMusicPage();
        yandexMusicPage.openMusicPage();
        yandexMusicPage.findMusic("Metal");
        yandexMusicPage.chooseMetallicaFromDropdown();
        YandexMusicResultOfSearchPage yandexMusicResultOfSearchPage = new YandexMusicResultOfSearchPage();
        String composerIsMetallica = yandexMusicResultOfSearchPage.getNameOfComposer();
        Assert.assertEquals(composerIsMetallica, "Metallica");
        boolean resultOfCheck =yandexMusicResultOfSearchPage.checkWhetherAlbumComposerIsTheSameAsRequested("Metallica");
        Assert.assertEquals(resultOfCheck, true);
    }
}
