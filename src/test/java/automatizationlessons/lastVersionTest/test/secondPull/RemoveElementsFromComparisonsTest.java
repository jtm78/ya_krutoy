package automatizationlessons.lastVersionTest.test.secondPull;

import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexComparisonsPage;
import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexMarketPage;
import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexMarketResultOfSearchPage;
import automatizationlessons.lastVersionTest.core.BaseTest;
import automatizationlessons.lastVersionTest.utils.Link;
import automatizationlessons.lastVersionTest.wrapperClasses.SetLink;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class RemoveElementsFromComparisonsTest extends BaseTest {

   private List<String> expectedListWithTwoFirstItems;
   private List<String> actualListWithTwoFirstItems;

    @Test
    public void comparionsIsEmpty(){
        SetLink.openLink(Link.YANDEX_URL.getLink());
        YandexMarketPage yandexMarketPage =new YandexMarketPage();
        yandexMarketPage.openMarketPage();
        yandexMarketPage.findItem("Note 8");
        YandexMarketResultOfSearchPage yandexMarketResultOfSearchPage = new YandexMarketResultOfSearchPage();
        expectedListWithTwoFirstItems = yandexMarketResultOfSearchPage.addTwoItemsIntoCompare();
        System.out.println(expectedListWithTwoFirstItems);
        yandexMarketResultOfSearchPage.openComparisons();
        YandexComparisonsPage yandexComparisonsPage = new YandexComparisonsPage();
        actualListWithTwoFirstItems = yandexComparisonsPage.nameOfTwoItemsFromComparisons();
        System.out.println(actualListWithTwoFirstItems);
        Assert.assertEquals(expectedListWithTwoFirstItems, actualListWithTwoFirstItems, "Добавлены разные товары");
        yandexComparisonsPage.deleteAllElements();
        String actualResultFromList = yandexComparisonsPage.getTextThatListIsEmpty();
        System.out.println(actualResultFromList);
        String expectedTextForEmptyList = "Товаров нет";
        Assert.assertTrue(actualResultFromList.contains("Товаров нет"), expectedTextForEmptyList);


    }
}
