package automatizationlessons.lastVersionTest.test.secondPull;

import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexActionsCamerasPage;
import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexMarketPage;
import automatizationlessons.lastVersionTest.core.BaseTest;
import automatizationlessons.lastVersionTest.utils.Link;
import automatizationlessons.lastVersionTest.wrapperClasses.SetLink;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class FilterByPriceTest extends BaseTest {

    @Test(description = "To filter cameras by price")
    public void filterByPrice() {

        SetLink.openLink(Link.YANDEX_URL.getLink());
        YandexMarketPage yandexMarketPage = new YandexMarketPage();
        yandexMarketPage.openMarketPage();
        yandexMarketPage.openActionsCameraFromElectronics();
        YandexActionsCamerasPage yandexActionsCamerasPage = new YandexActionsCamerasPage();
        yandexActionsCamerasPage.sortByPrice();
        List<Integer> actualSortedListByPrices = yandexActionsCamerasPage.getListWithSortedItemsByCite();
        List<Integer> expectedSortedListByPrices = yandexActionsCamerasPage.expectedList();
        Assert.assertEquals(expectedSortedListByPrices, actualSortedListByPrices, "Сортировка выполнена неправильно");
    }
}
