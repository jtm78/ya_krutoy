//package automatizationlessons.lastVersionTest.test.firstPull;
//
//import automatizationlessons.lastVersionTest.pagesAndActions.andersenLab.LogInFormActions;
//import automatizationlessons.lastVersionTest.pagesAndActions.andersenLab.MainPageActions;
//import automatizationlessons.lastVersionTest.pagesAndActions.andersenLab.UserPageActions;
//import automatizationlessons.lastVersionTest.pagesAndActions.andersenLab.VerificationPageActions;
//import automatizationlessons.lastVersionTest.core.BaseTest;
//import automatizationlessons.lastVersionTest.utils.Link;
//import automatizationlessons.lastVersionTest.wrapperClasses.SetLink;
//import org.testng.Assert;
//import org.testng.annotations.Test;
//
//public class LogInWithTokenTest extends BaseTest {
//
//    @Test
//
//    public void logInWithUsingToken(){
//        SetLink.openLink(Link.ANDERSENLAB.getLink());
//        MainPageActions.clickOnSignInIcon();
//        LogInFormActions.logIn();
//        VerificationPageActions.verifyToken();
//        String selectClassActionDropdown = UserPageActions.checkUserIsLogged();
//        Assert.assertEquals("Select Class Action", selectClassActionDropdown,"User is not logged");
//    }
//}
