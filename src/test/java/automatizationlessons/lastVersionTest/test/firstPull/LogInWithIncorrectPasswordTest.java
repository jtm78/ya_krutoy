package automatizationlessons.lastVersionTest.test.firstPull;

import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexMainPageActions;
import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.yandexLogin.LogInPageActions;
import automatizationlessons.lastVersionTest.core.BaseTest;
import automatizationlessons.lastVersionTest.utils.Link;
import automatizationlessons.lastVersionTest.wrapperClasses.SetLink;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LogInWithIncorrectPasswordTest extends BaseTest {

//    private static boolean windowAboutIncorrectPasswordDisplays= true;


    @Test
    public  void logInWhenPasswordIsIncorrect(){
        boolean expectedIsWindowDisplayed = true;
        SetLink.openLink(Link.YANDEX_URL.getLink());
        YandexMainPageActions yandexMainPageActions = new YandexMainPageActions();
        yandexMainPageActions.clickOnLoginButton();
        LogInPageActions logInPageActions = new LogInPageActions();
        logInPageActions.inputLogin("AutotestUser");
        logInPageActions.inputPassword("NoAutotestUser123");
        boolean passwordIsIncorrectWindowDisplays = logInPageActions.whetherIncorrectPasswordWindowDisplays();
        Assert.assertEquals(expectedIsWindowDisplayed,  passwordIsIncorrectWindowDisplays, "Окно о некорректном пароле не отображается");
    }
}
