package automatizationlessons.lastVersionTest.test.firstPull;

import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexMainPageActions;
import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.yandexLogin.LogInPageActions;
import automatizationlessons.lastVersionTest.core.BaseTest;
import automatizationlessons.lastVersionTest.utils.Link;
import automatizationlessons.lastVersionTest.wrapperClasses.SetLink;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LogInWithInvalidEmailTest extends BaseTest {

    @Test
    public  void windowAboutInvalidLoginDisplays(){
        SetLink.openLink(Link.YANDEX_URL.getLink());
        YandexMainPageActions yandexMainPageActions = new YandexMainPageActions();
        yandexMainPageActions.clickOnLoginButton();
        LogInPageActions logInPageActions = new LogInPageActions();
        logInPageActions.inputLogin("NoAutotestUser");
       String errorMessageText = logInPageActions.getTextFromInvalidLoginWindow();
        Assert.assertEquals("Такого аккаунта нет", errorMessageText, "Данный логин существует");
    }
}
