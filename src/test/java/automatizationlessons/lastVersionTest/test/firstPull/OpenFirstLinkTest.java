//package automatizationlessons.lastVersionTest.test.firstPull;
//
//import automatizationlessons.lastVersionTest.pagesAndActions.bbcPages.ResultOfSearchActions;
//
//import automatizationlessons.lastVersionTest.pagesAndActions.bbcPages.SearchPageActions;
//
//import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.yandexLogin.LogInPageActions;
//import automatizationlessons.lastVersionTest.core.BaseTest;
//import automatizationlessons.lastVersionTest.utils.Link;
//import automatizationlessons.lastVersionTest.wrapperClasses.SetLink;
//import org.testng.annotations.Test;
//
//public class OpenFirstLinkTest extends BaseTest {
//
//
//    @Test(priority = 1)
//    public void OpenFirstLink() {
//        SetLink.openLink(Link.BBC_URL.getLink());
//        SearchPageActions.fillSearchField("obama");
//        SearchPageActions.submitSearchField();
//        ResultOfSearchActions.openFirstLink();
//
//    }
//}
