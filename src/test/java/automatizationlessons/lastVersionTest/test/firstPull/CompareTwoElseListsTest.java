package automatizationlessons.lastVersionTest.test.firstPull;

import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexChooseCityActions;
import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexMainPageActions;
import automatizationlessons.lastVersionTest.core.BaseTest;
import automatizationlessons.lastVersionTest.utils.Link;
import automatizationlessons.lastVersionTest.wrapperClasses.SetLink;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class CompareTwoElseListsTest extends BaseTest {


    private List<String> londonListWithTextItems;
    private List<String> parisListWithTextItems;


    @Test
    public void CompareTwoListsWithElseItems() {
        SetLink.openLink(Link.YANDEX_URL.getLink());
        YandexMainPageActions yandexMainPageActions = new YandexMainPageActions();
        yandexMainPageActions.clickOnLocation();
        YandexChooseCityActions yandexChooseCityActions = new YandexChooseCityActions();
        yandexChooseCityActions.setCityLondon("Лондон");
        yandexMainPageActions.openMoreDropdown();
        londonListWithTextItems = yandexMainPageActions.rememberWebElementListToString();
//        londonListWithTextItems = YandexMainPageActions.setStringList(londonList);
        yandexMainPageActions.clickOnLocation();
        yandexChooseCityActions.setCityParis("Париж");
        yandexMainPageActions.openMoreDropdown();
        parisListWithTextItems = yandexMainPageActions.rememberWebElementListToString();
        System.out.println(londonListWithTextItems.equals(parisListWithTextItems));
//        parisListWithTextItems = YandexMainPageActions.setStringList(parisList);
        Assert.assertEquals(londonListWithTextItems, parisListWithTextItems, "Листы не одинаковые");



    }
}
