package automatizationlessons.lastVersionTest.test.firstPull;

import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexMainPageActions;
import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.yandexLogin.LogInPageActions;
import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.yandexLogin.UsersPage;
import automatizationlessons.lastVersionTest.core.BaseTest;
import automatizationlessons.lastVersionTest.utils.Link;
import automatizationlessons.lastVersionTest.wrapperClasses.SetLink;
import org.testng.Assert;
import org.testng.annotations.Test;

public class YandexLoginTest extends BaseTest {

    @Test
    public void logInYandex(){
        SetLink.openLink(Link.YANDEX_URL.getLink());
        YandexMainPageActions yandexMainPageActions = new YandexMainPageActions();
        yandexMainPageActions.clickOnLoginButton();
        LogInPageActions logInPageActions = new LogInPageActions();
        logInPageActions.inputLogin("AutotestUser");
        logInPageActions.inputPassword("AutotestUser123");
        UsersPage usersPage = new UsersPage();
        String userName = usersPage.getClientName();
        Assert.assertEquals("AutotestUser", userName, "Клиент не является AutotestUser");

    }
}
