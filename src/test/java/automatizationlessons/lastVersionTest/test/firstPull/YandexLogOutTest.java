package automatizationlessons.lastVersionTest.test.firstPull;

import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexMainPageActions;
import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.yandexLogin.LogInPageActions;
import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.yandexLogin.UsersPage;
import automatizationlessons.lastVersionTest.core.BaseTest;
import automatizationlessons.lastVersionTest.utils.Link;
import automatizationlessons.lastVersionTest.wrapperClasses.SetLink;
import org.testng.Assert;
import org.testng.annotations.Test;

public class YandexLogOutTest extends BaseTest {

    @Test
    public void yandexLogOut(){
        SetLink.openLink(Link.YANDEX_URL.getLink());
        YandexMainPageActions yandexMainPageActions = new YandexMainPageActions();
        yandexMainPageActions.clickOnLoginButton();
       LogInPageActions logInPageActions = new LogInPageActions();
        logInPageActions.inputLogin("AutotestUser");
        logInPageActions.inputPassword("AutotestUser123");
        UsersPage usersPage = new UsersPage();
        usersPage.logOut();
        String textFromLogInButton = yandexMainPageActions.getLogInText();
        Assert.assertEquals("Войти в почту", textFromLogInButton, "Юзер не разлогинен" );
    }
}
