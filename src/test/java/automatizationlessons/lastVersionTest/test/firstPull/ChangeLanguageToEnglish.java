package automatizationlessons.lastVersionTest.test.firstPull;

import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.YandexMainPageActions;
import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.ChooseLanguagePage;
import automatizationlessons.lastVersionTest.core.BaseTest;
import automatizationlessons.lastVersionTest.utils.Link;
import automatizationlessons.lastVersionTest.wrapperClasses.SetLink;
import org.testng.annotations.Test;

public class ChangeLanguageToEnglish extends BaseTest {

    @Test
    public void checkLanguageChangesToEnglish(){
        SetLink.openLink(Link.YANDEX_URL.getLink());
        YandexMainPageActions yandexMainPageActions = new YandexMainPageActions();
        yandexMainPageActions.openChooseLanguagePage();
        ChooseLanguagePage chooseLanguagePage = new ChooseLanguagePage();
        chooseLanguagePage.chooseLanguage();


    }
}
