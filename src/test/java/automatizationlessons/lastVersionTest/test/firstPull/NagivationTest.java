package automatizationlessons.lastVersionTest.test.firstPull;

import automatizationlessons.lastVersionTest.pagesAndActions.yandexPages.*;
import automatizationlessons.lastVersionTest.core.BaseTest;
import automatizationlessons.lastVersionTest.utils.Link;
import automatizationlessons.lastVersionTest.wrapperClasses.SetLink;
import automatizationlessons.lastVersionTest.wrapperClasses.SwitchToBeforeTab;
import org.testng.Assert;
import org.testng.annotations.Test;

public class NagivationTest extends BaseTest {

    @Test

    public void navigation() {
        SetLink.openLink(Link.YANDEX_URL.getLink());
        YandexPictures yandexPictures = new YandexPictures();
        yandexPictures.openImagesPage();
        String imagesUrl = yandexPictures.imagesCurrentUrl();
        Assert.assertEquals(imagesUrl, "https://yandex.by/images/", "Переход осуществлен не туда");
        VideoPage videoPage =new VideoPage();
        videoPage.openVideoPage();
        String videoUrl = videoPage.getCurrentVideoUrl();
        Assert.assertTrue(videoUrl.contains("https://yandex.by/video/"), "https://yandex.by/video/");
        YandexNewsPage yandexNewsPage = new YandexNewsPage();
        yandexNewsPage.openNewsPage();
        String newsUrl = yandexNewsPage.getCurrentNewsUrl();
        Assert.assertTrue(newsUrl.contains("https://news.yandex.by"), "https://news.yandex.by");
        YandexMapsPage yandexMapsPage = new YandexMapsPage();
        yandexMapsPage.openMapPage();
        String mapsUrl = yandexMapsPage.currentMapUrl();
        Assert.assertTrue(mapsUrl.contains("https://yandex.by/maps/"), "https://yandex.by/maps/");
        SwitchToBeforeTab switchToBeforeTab = new SwitchToBeforeTab();
        switchToBeforeTab.switchToTabNumberOne();
        YandexMarketPage yandexMarketPage = new YandexMarketPage();
        yandexMarketPage.openMarketPage();
        String marketUrl = yandexMarketPage.currentMarketUrl();
        Assert.assertTrue(marketUrl.contains("https://market.yandex.by"), "https://market.yandex.by");
        switchToBeforeTab.switchToTabNumberOne();
        TranslationPage translationPage = new TranslationPage();
        translationPage.openTranslationPage();
        String translationUrl = translationPage.currentTranslationUrl();
        Assert.assertTrue(translationUrl.contains("https://translate.yandex.by"), "https://translate.yandex.by");
        switchToBeforeTab.switchToTabNumberOne();
        YandexMusicPage yandexMusicPage = new YandexMusicPage();
        yandexMusicPage.openMusicPage();
        String musicUrl = yandexMusicPage.currentMusicUrl();
        Assert.assertTrue(musicUrl.contains("https://music.yandex.by/"), "https://music.yandex.by/");


    }
}
