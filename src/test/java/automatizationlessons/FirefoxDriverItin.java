package automatizationlessons;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxDriverItin {

    private static FirefoxDriverItin initize = null;

    private WebDriver driversecond;

    private FirefoxDriverItin(){
        System.setProperty("webdriver.gecko.driver", "/home/anduser/Загрузки/beer/geckodriver");
        driversecond = new FirefoxDriver();
    }

    public static FirefoxDriverItin getInstance(){
        if(initize==null){
            initize = new FirefoxDriverItin();
        }

        return initize;
    }

    public WebDriver getDriver(){
        return driversecond;
    }

    public static void enabled(WebElement locatorone) throws InterruptedException {

        for(int count = 0;;count++){
            if(count>=10){
                throw new TimeoutException();
            }
            try{
                System.out.println(locatorone.isDisplayed());
                break;
            }
            catch (NoSuchElementException e){
                Thread.sleep(1000);
            }
        }
    }

    public static void displayed(WebElement locatortwo) throws InterruptedException {

        for(int count = 0;;count++){
            if(count>=10){
                throw new TimeoutException();
            }
            try{
                System.out.println(locatortwo.isEnabled());
                break;
            }
            catch (NoSuchElementException e){
                Thread.sleep(1000);
            }
        }
    }
}
