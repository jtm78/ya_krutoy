import java.util.HashMap;
import java.util.Map;


public class Collection {
    public static void main(String[] args) {
        Map <Integer, String> map = new HashMap<Integer, String>();
        map.put(1, "Объект один");
        map.put(2, "Объект два");
        map.put(3, "Объект три");
        for (Map.Entry vivod : map.entrySet()){
            System.out.println("Ключ "+vivod.getKey()+" Значение "+vivod.getValue());
        }
        
    }
}
