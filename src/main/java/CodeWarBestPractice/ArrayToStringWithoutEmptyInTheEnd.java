package CodeWarBestPractice;

public class ArrayToStringWithoutEmptyInTheEnd {
    public static void main(String[] args) {

        TogetherToString together = new TogetherToString();
        String wordsTogether = together.vernut(new String[]{"one", "two", "three"});
        System.out.println(wordsTogether);

    }
}



class TogetherToString{


    public String vernut(String... words){
        StringBuilder builder = new StringBuilder();
        for(int i=0; i<words.length; i++){
            builder.append(words[i]);
            if(i+1<words.length){
                builder.append(" ");
            }

        }
        String result = builder.toString();
        return result;
        /*Более хорошее решение
        return String.join(" ", words);
         */
    }
}