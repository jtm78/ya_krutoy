package CodeWarBestPractice;

import java.util.Arrays;

public class BackSumMassiv {
    public static void main(String[] args) {
        Codewar codewar = new Codewar();
        System.out.println(codewar.sum(new int[]{1,2,3,4,6}));
    }
}


class Codewar{

    public String sum(int[] array){
        return Arrays.stream(array).sum()%2==0?"Even":"Odd";
    }
}