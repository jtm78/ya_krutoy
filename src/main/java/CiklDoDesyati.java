import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CiklDoDesyati  {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        for (; true;){
            int a = Integer.parseInt(reader.readLine());
            if (a<10){
                System.out.println("Введенное число меньше 10");
                break;
            }
            System.out.println(a);
        }
    }
}
