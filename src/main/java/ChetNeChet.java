import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ChetNeChet {
    public static void main(String[] args)throws  Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        if (n%2==0){
            System.out.println("Число четное");
        }
        else if(n%2!=0){
            System.out.println("Число нечетное");
        }
    }
}
