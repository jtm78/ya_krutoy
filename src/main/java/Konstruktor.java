public class  Konstruktor {
    public static void main(String[] args) {
        People human1 = new People();
    }
}
class People {
    private String name;
    private int age;

    public People(){ // имя конструктора всегда должно совпадать с именем класса (Human) и у контрутора нет
                    // типа возвращаемого значения
        this.name = "Default name";
        this.age = 15;
    }

    public People(String name){
        System.out.println("Второй конструктор");
        this.name= name;
    }
    public People(String name, int age){
        System.out.println("Третий контруктор");
        this.name=name;
        this.age=age;
    }
}
