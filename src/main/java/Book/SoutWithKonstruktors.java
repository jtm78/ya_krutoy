package Book;

public class SoutWithKonstruktors {
    public static void main(String[] args) {
        House house = new House();
        house.getWindow().StringOne();
    }
}

class Window{

    public String string = "дикий";


    public void StringOne(){

    }


    Window(int marker){
        System.out.println("Window("+ marker + ")");
    }
}

class House{
    /*переменная проинициализированная*/ public String string = "дикий";

   /*перменная проинициализированная*/ private Window window = new Window(1);
   /*переменная непроинициазиированная*/ private Window windowtwo;


    public Window getWindow(){
        return window;
    }

   private Window w1 = new Window(1);
}
