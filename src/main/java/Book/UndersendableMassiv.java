package Book;

public class UndersendableMassiv {
    public static void main(String[] args) {
        int [] a1 = new int[]{1,2,3,4,5};
        int[] a2;
        a2 = a1.clone();

        for(int i=0; i<a2.length; i++){
            a2[i] = a2[i]+1;
            System.out.println(a2[i]);
        }
        for (int i=0; i<a1.length; i++){
            System.out.println("a1[" +i + "] = " + a1[i]);
        }

        int x = 20;
        int y = x;

        y+=10;


        System.out.println(x);
        System.out.println(y);

        int[] b1 = new int[]{1,2,3,4,5};
        int[] b2;
        b2 = b1;
        int[] b3 =b2;
        for (int i=0; i<b2.length; i++){
            b2[i] = b2[i] + 1;
        }
        for (int i=0; i<b1.length; i++){
            System.out.println("b1[" +i + "] = " + b1[i]);
        }

        for (int i=0; i<b3.length; i++){
            System.out.println("b3[" +i + "] = " + b3[i]);
        }
    }
}
