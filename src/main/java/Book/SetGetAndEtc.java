package Book;

public class SetGetAndEtc {
    public static void main(String[] args) {
        ForTesting forTesting = new ForTesting();
//        forTesting.setValue();
        int a = 0;
        forTesting.setTwo(a);
        System.out.println(a);

        String c = null;
        System.out.println(forTesting.setFive(c));

        String b = new String();
        b = forTesting.setFive(b);

        System.out.println(b);

//        int a = forTesting.setOne();
//        System.out.println("Тут должно быть 20 " + a);
//        int b = forTesting.setTwo(a);
//        System.out.println("Тут должно быть 30 " + b);
//        int c = forTesting.setThree();
//        System.out.println("Тут должно быть 40 " + c);


    }
}

class ForTesting{
    private int y;



    public void setValue() {
        y = 10;
        System.out.println("Set value выводит " + y);
    }

    public int getY(){
        return y;
    }

    public int setOne(){
        y =getY()+10;
        return y;
    }
    public int setTwo(int set){
        int lean=set+10;
        return lean;
    }

    public int setThree(){
        y= getY()+10;
        return y;
    }
    public String setFive(String five){

        return five+"весело";
    }
}
