package Book.nailNewCourses;

public class MajorWordThis {
    public static void main(String[] args) {
        DeleteTwo deleteOne = new DeleteTwo(10,"Vlad");
        DeleteTwo deleteTwo = new DeleteTwo(20, "Vika");
        deleteOne.getint();
        deleteTwo.getint();
        System.out.println(deleteOne);
        System.out.println(deleteTwo);
    }
}
class DeleteTwo{
    private String name;
    private int age;

    public DeleteTwo(int age, String name) {
        this.name = name;
        this.age = age;
    }

    public int getint(){
        age+=10;
        return age;
    }


    @Override
    public String toString() {
        return "DeleteTwo{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
