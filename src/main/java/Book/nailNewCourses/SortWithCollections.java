package Book.nailNewCourses;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortWithCollections {
    public static void main(String[] args) {
        List<String> animals = new ArrayList<>();
        animals.add("cat");
        animals.add("bird");
        animals.add("elephant");
        animals.add("dog");
        //тут мы доавбляем в аргументы свою сортировку new StringLenghSort
        Collections.sort(animals, new StringLengthSort());
        System.out.println(animals);
    }
}

class StringLengthSort implements Comparator<String> {


    @Override
    public int compare(String o1, String o2) {
        if(o1.length()>o2.length()){
            return 1;
        }
        else if(o1.length()<o2.length()){
            return -1;
        }
        else {
            return 0;
        }
    }
}
