package Book.nailNewCourses;

import java.util.HashSet;
import java.util.Set;

public class TwoSetsTogether {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(0);
        set1.add(1);
        set1.add(2);
        set1.add(4);

        Set<Integer> set2 = new HashSet<>();
        set2.add(4);
        set2.add(5);
        set2.add(6);

        /*Этот сет нужен для того, чтобы объединить 2 вышестоящих сета
        Мы передаем в конструктор первый сет
        И потом в данный сет ещё доавбялет второй сет
        В данном случае у нас выводятся только числа, которые не повторяются
        Объединение множеств
         */
        Set<Integer> twoSetTogether = new HashSet<>(set1);
        twoSetTogether.addAll(set2);
        System.out.println(twoSetTogether);


        /*пересечение множеств - выводит только совпадения cо 2 коллекцией*/
        Set<Integer> peresechenie = new HashSet<>(set1);
        peresechenie.retainAll(set2);
        System.out.println(peresechenie);

        /*данная коллекция беерт значения другой коллекции*/
        Set<Integer> perhaps = new HashSet<>(set1);
        System.out.println(perhaps);

        /*Разность - показывает только разные элементы*/
        Set<Integer> raznost = new HashSet<>(set1);
        raznost.removeAll(set2);
        System.out.println(raznost);
    }
}
