package Book.nailNewCourses;

import java.util.*;

public class HashCodeAndEtc {
    public static void main(String[] args) {
        Map<Person, String> map = new HashMap<>();
        Set<Person> set = new HashSet<>();

        Person person1 = new Person(10, "Kate");
        Person person2 = new Person(10, "Kate");
        person1.equals(person2);


        map.put(person1, "123");
        map.put(person2,"456");

        set.add(person1);
        set.add(person2);

        System.out.println(map);
        System.out.println(set);
    }
}


class Person{

    private int age;
    private String name;

    public Person(int age, String name) {
        this.age = age;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", name=" + name +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, name);
    }
}