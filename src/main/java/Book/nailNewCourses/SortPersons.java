package Book.nailNewCourses;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortPersons {

    public static void main(String[] args) {
        List<PersonRepeat> people = new ArrayList<>();
        people.add(new PersonRepeat(10, "Bob"));
        people.add(new PersonRepeat(9, "Tyler"));
        people.add(new PersonRepeat(20, "Jake"));

        Collections.sort(people, new Comparator<PersonRepeat>() {
            @Override
            public int compare(PersonRepeat o1, PersonRepeat o2) {
                if(o1.getAge()>o2.getAge()){
                    return 1;
                }else if(o1.getAge()<o2.getAge()){
                    return -1;
                }
                else {
                    return 0;
                }
            }
        });
        System.out.println(people);
    }


}

class PersonRepeat {
    private int age;
    private String name;


    public PersonRepeat(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "PersonRepeat{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
