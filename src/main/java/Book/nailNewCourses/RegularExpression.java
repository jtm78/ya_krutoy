package Book.nailNewCourses;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class RegularExpression {
    public static void main(String[] args) {
        /*  \\d - ОДНА цифра  */
        String withOneLetter = "a";
        /* Тут будет false, потому что a - Это не цифра, а буква */
        System.out.println(withOneLetter.matches("\\d"));
        String withOneNumber = "1";
        /* Тут будет true, потому что 1 - это цифра */
        System.out.println(withOneNumber.matches("\\d"));

        /* для сравнения нескольких цифр используется:
               * - 0  или более (Пример: \\d* );
               + - 1 или более (пример: \\d+ );
               ? - 0 или 1 символов до. Нужно для того, чтобы не было зависмости от знака
               например может быть 123 а может быть -123
               поэтому нам нужно сделать ?-\\d+
         */
        String aFewNumbers = "1234";
        System.out.println(aFewNumbers.matches("\\d+"));

        /*если же у нас перед нашим числом есть какие-то буквы, то необходимо сделать следующее:
        [a-zA-Z] - тут мы делаем, что любые буквы от a до z и верхнего регистра англйиского алфавита
        могут присутствовать

        для цифр делается - [0-9]
         */
        String withAnyLetter = "a11234";

        /* . - это любой символ. Ниже пример на проверку валидации сайта*/
        String siteUrl = "http://www.google.com";
        System.out.println(siteUrl.matches("http://www\\..+\\.(com|ru)"));

    }
}

