package Book.nailNewCourses;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularWithPattern {
    public static void main(String[] args) {
        String text = "Hello guys there are emailFirst@mail.ru a few emails emailSecond@gmail.com";
        Pattern pattern = Pattern.compile("\\w+@(mail|gmail)\\.(ru|com)");
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()){
            System.out.println(matcher.group());
        }
    }
}
