package Book.nailNewCourses;

import java.util.Objects;

public class EqualsWithMyClass {
    public static void main(String[] args) {
        ForEquals firstObject = new ForEquals(10, "Misha");
        ForEquals secondObject = new ForEquals(10, "Misha");
        System.out.println(firstObject.equals(secondObject));
    }
}


class ForEquals{
    private int id;
    private String name;


    public ForEquals(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ForEquals forEquals = (ForEquals) o;
        return id == forEquals.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
