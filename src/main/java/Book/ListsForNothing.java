package Book;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListsForNothing {
    private static List<String> list1;
    private static List<String> list2;

    public static void main(String[] args) {
        list1 = GetList.fillList();
        list2 = GetList.fillList();
        System.out.println(list1);
        System.out.println(list2);

        GetList vernost = new GetList();
        String s = vernost.vernut(new String[]{"one","two"});
        System.out.println(s);

        int[] massiv = new int[10];
        for (int i=0;i<massiv.length; i++){
            massiv[i] =i;
        }

    }
}

class GetList{

    public static List<String> fillList(){
        List<String> fill = new ArrayList<>();
        fill.add("1");
        fill.add("2");
        return fill;
    }

    public String vernut(String... words){
        StringBuilder builder = new StringBuilder();
        for(int i=0; i<words.length; i++){
            builder.append(words[i]);
            if(i+1<words.length){
               builder.append(" ");
            }

        }
        String s = builder.toString();
        return s;
    }
}
