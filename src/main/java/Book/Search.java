package Book;
public class Search {
    public static void main(String[] args) {
        Search search = new Search();
        search.method(20,15);
        System.out.println(search.getItem());
    }

    private int item;

    public void method(int list, int item){

        this.item = item;
        int low = 0;
        int high = list;
        while(low<=high){
            int mid = low+high;
            int guess = mid ;
            if (guess == item){
                this.item = guess;
                break;
            }
            else if (guess<item){
                low = mid+1;
            }
            else {
                high = mid-1;
            }
        }
    }

    public int getItem(){
        return item;
    }

}




