package mnogopotochnost;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DomashkaOne {
    public static void main(String[] args) throws InterruptedException {
            new Threads().forMain();
    }
}

class Threads{
    private Object object1 = new Object();
    private Object object2 = new Object();

    private List<Integer> list1 = new ArrayList<>();
    private List<Integer> list2 = new ArrayList<>();

    public void zapolneniyeList1() throws InterruptedException {
        synchronized (object1) {
            for (int i = 0; i < 1000; i++) {
                Thread.sleep(1);
                list1.add(i);
            }
        }
    }

    public void zapolneniyeList2() throws InterruptedException {
        synchronized (object2) {
            for (int i = 0; i < 1000; i++) {
                Thread.sleep(1);
                list2.add(i);
            }
        }
    }

    public  void twoMethodsTogether() throws InterruptedException {
        zapolneniyeList1();
        zapolneniyeList2();
    }

    public void forMain() throws InterruptedException {
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    twoMethodsTogether();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    twoMethodsTogether();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        System.out.println(list1.size());
        System.out.println(list2.size());
    }

}
