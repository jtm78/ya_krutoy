package mnogopotochnost;

import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.Scanner;

public class SecondLesson {
    public static void main(String[] args) {
        MyThread thread = new MyThread();
        thread.start();

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        thread.shutdown();

    }
}

class MyThread extends Thread{
    private volatile boolean running = true;


    public void run(){
        while(running){
            System.out.println("Coin");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void shutdown(){
        this.running = false;
    }
}