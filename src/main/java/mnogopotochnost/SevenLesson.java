package mnogopotochnost;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class SevenLesson {
    private Queue<Integer> queue = new LinkedList();
    private final int LIMIT = 10;
    private Object lock = new Object();

    public static void main(String[] args) throws InterruptedException {
        ProducerConsumer pc = new ProducerConsumer();


        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    pc.produce();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    pc.consume();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();
    }
}


class ProducerConsumer {

    public void produce() throws InterruptedException {
        synchronized (this) {
            System.out.println("Producer thread started....");
            wait(); // 1 - отдаем instinsic lock, 2 - ждем, пока будет вызван notify().
            System.out.println("Producer thread resumed...");
        }
    }

    public void consume() throws InterruptedException {
        Thread.sleep(2000);
        Scanner scanner = new Scanner(System.in);

        synchronized (this){
            System.out.println("Waiting for return pressed");
            scanner.nextLine();
            notify();
        }

    }
}