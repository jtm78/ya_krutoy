import java.io.BufferedReader;
import java.io.InputStreamReader;

public class DoWhile {
    public static void main (String[] args) throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int value;
        do {
            System.out.println("Введи число 10");
            value = Integer.parseInt(reader.readLine());
        }
        while (value!=10);
        System.out.println("Ввел 10");
    }
}
