public class ProgramChange {
    public static void main(String[] args) {
        NameOfHuman name1 = new NameOfHuman("Alice");
        System.out.println(name1.getName());
        changeName(name1);
        System.out.println(name1.getName());
    }
    static void changeName(NameOfHuman p){
        p.setName("Pasha");
    }
}

class NameOfHuman{
    private String name;

    public NameOfHuman(String name){
        this.name = name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }
}
