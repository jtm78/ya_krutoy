public class StaticClass {
    public static void main(String[] args) {
        New.description = "Napas";
        New.getDescription();

    }
}
class New {
    public static String description; /*С помощью Static мы обращаемся прямо к классу
                                       А не к переменной объекта */
    public static void getDescription(){
        System.out.println(description);
    }
}
