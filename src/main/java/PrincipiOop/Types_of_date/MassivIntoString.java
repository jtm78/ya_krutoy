package PrincipiOop.Types_of_date;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MassivIntoString {
    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("Hey ");
        list.add("Bob ");
        StringBuilder builder = new StringBuilder();
        for (String s : list) {
            builder.append(s);
        }

        String str = builder.toString();
        System.out.println(str);
        char[] back = str.toCharArray();
        for (int i = 0; i < back.length; i++) {
            System.out.println(back[i]);
        }
    }
}

