package PrincipiOop.Types_of_date;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Factorial {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = 1;
        for (int i=1;i<=a; i++){
            b= b*i;
        }
        System.out.println(b);
    }
}
