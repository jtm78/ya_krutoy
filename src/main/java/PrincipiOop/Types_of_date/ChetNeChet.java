package PrincipiOop.Types_of_date;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ChetNeChet {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int i = Integer.parseInt(reader.readLine());
        String s = (i%2==0)?"четное":"нечетное";
        System.out.println(s);
        ChetNeChet chet1 = new ChetNeChet();
        ChetNeChet chet2 = new ChetNeChet();
        System.out.println(chet1.hashCode() == chet2.hashCode());
        System.out.println(chet1.toString());
    }
}
