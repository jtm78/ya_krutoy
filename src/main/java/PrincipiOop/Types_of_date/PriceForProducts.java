package PrincipiOop.Types_of_date;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class PriceForProducts  {
    public static void main(String[] args) throws Exception {
        System.out.println("Введите код товара");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        for(; true; ) {
            int kod = Integer.parseInt(reader.readLine());
            if (kod == 10) {
                System.out.println("10 копеек за сахар");
                break;
            } else if (kod == 100) {
                System.out.println("100 копеек за батон");
                break;
            } else if (kod == 300) {
                System.out.println("300 копеек за чипсы");
                break;
            } else {
                System.out.println("Данный код неверен, введите код по-новой");

            }
        }
    }
}
