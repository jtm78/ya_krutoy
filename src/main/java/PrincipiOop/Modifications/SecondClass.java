package PrincipiOop.Modifications;

public class SecondClass extends FirstClass {
    @Override
    public void method1() {
        super.method1();
    }

    @Override
    protected void method2() {
        super.method2();
    }

    public static void method4(){
        System.out.println(2);
    }
}
