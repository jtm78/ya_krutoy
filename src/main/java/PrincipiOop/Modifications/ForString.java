package PrincipiOop.Modifications;

import java.util.ArrayList;
import java.util.List;

public class ForString {
    public static void main(String[] args) {
        Balcon back = new Balcon("имя", "фамилия");
        System.out.println(back.toString());
        List<String> massiv = new ArrayList<String>();
        massiv.add("A");
        massiv.add("b");

        System.out.println(massiv.get(1));

    }
}

    class Balcon {
        private String name;
        private String surname;

        public Balcon(String name, String surname){
            this.name = name;
            this.surname = surname;
        }


        public String toString(){
            return name+surname;
        }


    }


