package PrincipiOop.InterfaceAuto;

public class Test {
    public static void main(String[] args) {
        Sobaka dog = new Dog();
        dog.eat(1,1);
        dog.height();
        ((Dog) dog).move(1);
        dog.weight();
        dog.move();

    }
}
