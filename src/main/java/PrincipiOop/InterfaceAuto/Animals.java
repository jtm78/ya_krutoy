package PrincipiOop.InterfaceAuto;

public interface Animals {

    public void weight();

    public void height();

    public void move();

    public void eat(int i, int c);
}
