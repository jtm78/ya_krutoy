public class Abstracziya {
    public static void main(String[] args) {
        Employer vlad = new Employer("Vlad", "Andersen");
        vlad.dispaplay();
        Client artem = new Client("Artem", "SmartIT");
        artem.dispaplay();


    }
}

abstract class PersonTen {
    private String name;


    public PersonTen(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract void dispaplay();

}

class Employer extends PersonTen {
    private String company;

    public Employer(String name, String company) {
        super(name);
        this.company = company;
    }


    @Override
    public void dispaplay() {
        System.out.printf("Employee Name: %s \t Bank: %s \n", getName(), company);
    }
}

class Client extends PersonTen {
    private String company;

    public Client(String name, String company) {
        super(name);
        this.company = company;
    }

    @Override
    public void dispaplay() {
        System.out.printf("Client Name: %s \t Bank: %s \n", getName(), company);
    }
}