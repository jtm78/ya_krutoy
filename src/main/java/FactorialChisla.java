import java.io.BufferedReader;
import java.io.InputStreamReader;

public class FactorialChisla {
    static int calcul(int b){
        int c = 1;
        for (int i=1; i<=b; i++){
            c= c*i;
        }
        return c;
    }

    public static void main(String[] args)throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int c= Integer.parseInt(reader.readLine());
        System.out.println(calcul(c));
    }
}
