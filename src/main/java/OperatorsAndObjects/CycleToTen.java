package OperatorsAndObjects;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CycleToTen {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int firstinput = Integer.parseInt(reader.readLine());
        do {
            System.out.println(firstinput);
            firstinput--;

        } while (firstinput>10);
        int secondinput = Integer.parseInt(reader.readLine());
        while (secondinput>10){
            System.out.println(secondinput);
            secondinput--;
        }
    }
}
