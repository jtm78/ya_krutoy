package OperatorsAndObjects;


import java.io.BufferedReader;
import java.io.InputStreamReader;

public  class MethodFactorial {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int g = Integer.parseInt(reader.readLine());
        Factorial factorial = new Factorial(g);
        factorial.getInfo();

    }
}

class Factorial {
    private int a;
    public Factorial(int a){

        int c = 1;
        for (int i=1; i<=a; i++){
           c=c*i;
        }
        this.a = c;

    }

    public void getInfo(){
        System.out.println(a);
    }


}

