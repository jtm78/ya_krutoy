package OperatorsAndObjects;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MethodFactorialSecond {
    static int calcul(int input){
        int factorial = 1;
        for (int i=1; i<=input; i++){
            factorial*=i;
        }
        return factorial;
    }

    public static void main(String[] args)throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int input = Integer.parseInt(reader.readLine());
        System.out.println(calcul(input));
    }
}

