package NewInterface;

import Interface.Info;

public class First implements NewInfo {
    public String name;

    public First(String name){
        this.name = name;
    }
    public void method(){
        System.out.println(name);
    }
}
