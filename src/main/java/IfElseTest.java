public class IfElseTest {
    public static void main(String[] args) {
        System.out.println(Podschet.method(10,10));

    }
}

class Podschet {
    static int method(int first, int second){
        if (first<second){
            return -1;
        }
        else if (first>second){
            return +1;
        }
        else {
            return 0;
        }
    }
}
