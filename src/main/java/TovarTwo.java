import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TovarTwo {
    public static void main(String[] args)throws Exception {
        System.out.println("Чтобы узнать цену за товар введите код");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int tovar = Integer.parseInt(reader.readLine());
        if (tovar==1){
            System.out.println("Цена батона 2 рубля");
        }
        else if (tovar==2){
            System.out.println("Цена за хлеб 4 рубля");
        }
        else if (tovar==3){
            System.out.println("Цена за молоко 5 рублей");
        }
        else if (tovar==4){
            System.out.println("Цена за кукурузу 1 рубль");
        }
        else {
            System.out.println("Цены на данный товар нет в базе данных");
        }

    }
}
