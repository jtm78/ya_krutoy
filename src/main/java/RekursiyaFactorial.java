public class RekursiyaFactorial {
    public static void main(String[] args) {
        System.out.println(method1(4));
    }



    //method(4);
    //method(3);
    //method(2);
    //method(1)
    public static int method1(int n){
        if(n==1){
            return 1;
        }

        return n*method1(n-1);
    }
}
