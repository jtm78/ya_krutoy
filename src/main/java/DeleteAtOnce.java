import java.util.ArrayList;
import java.util.List;

public class DeleteAtOnce {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("First");
        list.add("Second");
        DeleteAtOnce.obj(list);
    }
    public static void obj(List<String> listone){
        StringBuilder builder = new StringBuilder();
        for (String s : listone){
            builder.append(s);
        }
        String s = builder.toString();
        System.out.println(s);
    }
}
