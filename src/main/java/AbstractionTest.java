public class AbstractionTest {
    public static void main(String[] args) {
        Animalsagain rabbit = new Dikiy("зайчик" , "заяц");
        rabbit.display();

    }
}

abstract class Animalsagain{
    private String type;

    public Animalsagain(String type){

        this.type = type;
    }

    public String getType(){
        return type;
    }

    public abstract void display();
}

class Dikiy extends Animalsagain{
    private String district;

    public Dikiy(String type, String district){
        super(type);
        this.district = district;
    }


    @Override
    public void display() {
        System.out.printf("Имя" + getType() + "Фамилия" + district);
    }
}