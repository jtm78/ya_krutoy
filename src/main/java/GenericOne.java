import java.util.ArrayList;

public class GenericOne<T> {
    private T val;

    public GenericOne(T arg) {
        val = arg;
    }

    public String toString() {
        return "{" + val + "}";
    }

    public T getValue() {
        return val;
    }
}

class Test {
    public static void main(String[] args) {
        GenericOne<Integer> value1 = new GenericOne<Integer>(10);
        System.out.println(value1);
        Integer intValue1 = value1.getValue();
        GenericOne<String> value2 = new GenericOne<String>("Hello world");
        System.out.println(value2);
        String string = value2.getValue();
    }
}

