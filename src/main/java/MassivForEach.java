public class MassivForEach {
    public static void main (String[] args){
        int [] number = new int[5];
        for (int i=0;i<number.length; i++){
            number[i]=i*10;
        }
        int sum=0;
        for(int newint:number){
            sum=sum+newint;
        }
        System.out.println(sum );
    }
    public int[] number(int n){
        int[] massiv = new int[n];
        for (int i=n; n>0; n--){
            massiv[i]=n;
        }
        return massiv;

    }
}
