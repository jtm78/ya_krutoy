public class ClassesAndObjects {
    public static void main(String [] args) {
        Person person1 = new Person(); // новый объект класса Person // Person - тип данных
                                      // person1 - переменная; new Person () - новый объект класса Person
        person1.name = ("Влад");
        person1.age = 20;
        person1.speak();
        person1.speak();
        System.out.println(person1.setAge(10));



    }

}
class Person {
    String name;
    int age;

    int setAge (int newage){
        age = newage;
        return age;

    }

    int calculateYears(){
        int years = 65 - age;
        return years;
    }
    void speak (){
        System.out.println(calculateYears());

    }

}
