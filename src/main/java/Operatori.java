import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Operatori {
    public static void main(String[] args) throws Exception {
        System.out.println("Сколько сейчас времени?");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int time = Integer.parseInt(reader.readLine());
        if (time>22){
            System.out.println("Идем спать");
            return;
        }
        if(19<time&&time<22){
            System.out.println("Смотрим телевизор");
            return;
        }

        System.out.println("Сколько денег у вас есть?");
        int money = Integer.parseInt(reader.readLine());
        if (8<=time&&time<=12&&money>10){
            System.out.println("Идем в магазин");
        }
        else if(time>12&&money>50){
            System.out.println("Идем в кафе");

        }
        else if (time<19&&money<50){
            System.out.println("Идем к соседу");
        }


    }
}
