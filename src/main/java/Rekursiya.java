public class Rekursiya {
    public static void main(String[] args) {
        shitat(3);
    }

    //counter(3) -> counter(2) - > counter(1)


    // когда мы вызываем метод - мы идем сверху вниз, когда срабатаывает return - мы идем снизу вверх
    // counter(3)
    // counter(2)
    // counter(1)
    // counter(0)
    public static void shitat(int n){
        if(n==0){
            return;
        }
        System.out.println(n);
        shitat(n-1);
    }
}
