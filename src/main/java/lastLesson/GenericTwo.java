package lastLesson;

import java.util.ArrayList;
import java.util.List;


public class GenericTwo <T> {
    public static void main(String[] args) {
        List <String> object1 = new ArrayList<String>();
        System.out.println(union(object1, "Karl"));
        List <Integer> object2 = new ArrayList<Integer>();
        System.out.println(union(object2, 1));


    }


    public static <E> List<E> union(List<E> object, E value){
        List<E> result = new ArrayList<E>(object);
        result.add(value);
        return result;
    }
}
