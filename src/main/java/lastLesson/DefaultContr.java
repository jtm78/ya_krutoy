package lastLesson;

public class DefaultContr {
    public static void main(String[] args) {
        Konstruktor objectone = new Konstruktor();
        Konstruktor object = new Konstruktor(1);


    }
}

class Konstruktor extends DefaultContr{
    public Konstruktor(){
        System.out.println(getClass().getName());
    }
    public Konstruktor(int a){ //Чтобы не делать 2 класса, я просто поставил, чтобы конструктор принимал int, но возвращает он все равно имя суперкласса
        System.out.println(getClass().getSuperclass());
    }


}
