package lastLesson;

public class GenericThree {
    public static void main(String[] args) {
        Generation<Integer> objectOne = new Generation<Integer>(1);
        System.out.println(objectOne.getObject());
        Generation<String> objectTwo = new Generation<String>("Karl");
        System.out.println(objectTwo.getObject());
    }



}

class Generation<T>{
    T object;
    public Generation(T konstuktor){
        object = konstuktor;
    }
    public T getObject(){
        return object;
    }
}